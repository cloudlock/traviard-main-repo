'use strict';

const __auser = {uanme: "traviard_admin", pass: "1qaz2wsx@"};
const __suser = {uname: "traviard", pass: "spd@1990"};

let _db = db.getSiblingDB('admin');
_db.auth(__auser.uanme, __auser.pass);
_db.createUser(
    {
        user: __suser.uname,
        pwd: __suser.pass,
        roles: [{role: "readWrite", db: "traviard_secdb"}]
    }
);

const countries = [
    {
        "n": "Afghanistan",
        "cc": "+93",
        "ic": "af",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Åland Islands",
        "cc": "+358",
        "ic": "ax",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Albania",
        "cc": "+355",
        "ic": "al",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Algeria",
        "cc": "+213",
        "ic": "dz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "American Samoa",
        "cc": "+1-684",
        "ic": "as",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Andorra",
        "cc": "+376",
        "ic": "ad",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Angola",
        "cc": "+244",
        "ic": "ao",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Anguilla",
        "cc": "+1-264",
        "ic": "ai",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Antarctica",
        "cc": "+672",
        "ic": "aq",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Antigua and Barbuda",
        "cc": "+1-268",
        "ic": "ag",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Argentina",
        "cc": "+54",
        "ic": "ar",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Armenia",
        "cc": "+374",
        "ic": "am",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Aruba",
        "cc": "+297",
        "ic": "aw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Australia",
        "cc": "+61",
        "ic": "au",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Austria",
        "cc": "+43",
        "ic": "at",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Azerbaijan",
        "cc": "+994",
        "ic": "az",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bahamas",
        "cc": "+1-242",
        "ic": "bs",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bahrain",
        "cc": "+973",
        "ic": "bh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bangladesh",
        "cc": "+880",
        "ic": "bd",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Barbados",
        "cc": "+1-246",
        "ic": "bb",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Belarus",
        "cc": "+375",
        "ic": "by",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Belgium",
        "cc": "+32",
        "ic": "be",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Belize",
        "cc": "+501",
        "ic": "bz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Benin",
        "cc": "+229",
        "ic": "bj",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bermuda",
        "cc": "+1-441",
        "ic": "bm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bhutan",
        "cc": "+975",
        "ic": "bt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bolivia",
        "cc": "+591",
        "ic": "bo",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bosnia and Herzegovina",
        "cc": "+387",
        "ic": "ba",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Botswana",
        "cc": "+267",
        "ic": "bw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Brazil",
        "cc": "+55",
        "ic": "br",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "British Virgin Islands",
        "cc": "+1-284",
        "ic": "vg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Brunei",
        "cc": "+673",
        "ic": "bn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Bulgaria",
        "cc": "+359",
        "ic": "bg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Burkina Faso",
        "cc": "+226",
        "ic": "bf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Burundi",
        "cc": "+257",
        "ic": "bi",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cambodia",
        "cc": "+855",
        "ic": "kh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cameroon",
        "cc": "+237",
        "ic": "cm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Canada",
        "cc": "+1",
        "ic": "ca",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cape Verde",
        "cc": "+238",
        "ic": "cv",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cayman Islands",
        "cc": "+1-345",
        "ic": "ky",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Central African Republic",
        "cc": "+236",
        "ic": "cf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Chad",
        "cc": "+235",
        "ic": "td",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Chile",
        "cc": "+56",
        "ic": "cl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "China",
        "cc": "+86",
        "ic": "cn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Christmas Island",
        "cc": "+61",
        "ic": "cx",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cocos (Keeling) Islands",
        "cc": "+61",
        "ic": "cc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Colombia",
        "cc": "+57",
        "ic": "co",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Comoros",
        "cc": "+269",
        "ic": "km",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cook Islands",
        "cc": "+682",
        "ic": "ck",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Costa Rica",
        "cc": "+506",
        "ic": "cr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Croatia",
        "cc": "+385",
        "ic": "hr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cuba",
        "cc": "+53",
        "ic": "cu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Curaçao",
        "cc": "+599",
        "ic": "cw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Cyprus",
        "cc": "+357",
        "ic": "cy",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Czech Republic",
        "cc": "+420",
        "ic": "cz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "DR Congo",
        "cc": "+243",
        "ic": "cd",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Denmark",
        "cc": "+45",
        "ic": "dk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Djibouti",
        "cc": "+253",
        "ic": "dj",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Dominica",
        "cc": "+1-767",
        "ic": "dm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Dominican Republic",
        "cc": "+1",
        "ic": "do",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ecuador",
        "cc": "+593",
        "ic": "ec",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Egypt",
        "cc": "+20",
        "ic": "eg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "El Salvador",
        "cc": "+503",
        "ic": "sv",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Equatorial Guinea",
        "cc": "+240",
        "ic": "gq",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Eritrea",
        "cc": "+291",
        "ic": "er",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Estonia",
        "cc": "+372",
        "ic": "ee",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ethiopia",
        "cc": "+251",
        "ic": "et",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Falkland Islands",
        "cc": "+500",
        "ic": "fk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Faroe Islands",
        "cc": "+298",
        "ic": "fo",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Fiji",
        "cc": "+679",
        "ic": "fj",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Finland",
        "cc": "+358",
        "ic": "fi",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "France",
        "cc": "+33",
        "ic": "fr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "French Polynesia",
        "cc": "+689",
        "ic": "pf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Gabon",
        "cc": "+241",
        "ic": "ga",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Gambia",
        "cc": "+220",
        "ic": "gm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Georgia",
        "cc": "+995",
        "ic": "ge",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Germany",
        "cc": "+49",
        "ic": "de",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ghana",
        "cc": "+233",
        "ic": "gh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Gibraltar",
        "cc": "+350",
        "ic": "gi",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Greece",
        "cc": "+30",
        "ic": "gr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Greenland",
        "cc": "+299",
        "ic": "gl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Grenada",
        "cc": "+1-473",
        "ic": "gd",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guam",
        "cc": "+1-671",
        "ic": "gu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guatemala",
        "cc": "+502",
        "ic": "gt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guernsey",
        "cc": "+44-1481",
        "ic": "gg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guinea",
        "cc": "+224",
        "ic": "gn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guinea-Bissau",
        "cc": "+245",
        "ic": "gw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Guyana",
        "cc": "+592",
        "ic": "gy",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Haiti",
        "cc": "+509",
        "ic": "ht",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Honduras",
        "cc": "+504",
        "ic": "hn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Hong Kong",
        "cc": "+852",
        "ic": "hk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Hungary",
        "cc": "+36",
        "ic": "hu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Iceland",
        "cc": "+354",
        "ic": "is",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "India",
        "cc": "+91",
        "ic": "in",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Indonesia",
        "cc": "+62",
        "ic": "id",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Iran",
        "cc": "+98",
        "ic": "ir",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Iraq",
        "cc": "+964",
        "ic": "iq",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ireland",
        "cc": "+353",
        "ic": "ie",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Isle of Man",
        "cc": "+44-1624",
        "ic": "im",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Israel",
        "cc": "+972",
        "ic": "il",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Italy",
        "cc": "+39",
        "ic": "it",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ivory Coast",
        "cc": "+225",
        "ic": "ci",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Jamaica",
        "cc": "+1-876",
        "ic": "jm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Japan",
        "cc": "+81",
        "ic": "jp",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Jersey",
        "cc": "+44-1534",
        "ic": "je",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Jordan",
        "cc": "+962",
        "ic": "jo",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Kazakhstan",
        "cc": "+7",
        "ic": "kz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Kenya",
        "cc": "+254",
        "ic": "ke",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Kiribati",
        "cc": "+686",
        "ic": "ki",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Kuwait",
        "cc": "+965",
        "ic": "kw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Kyrgyzstan",
        "cc": "+996",
        "ic": "kg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Laos",
        "cc": "+856",
        "ic": "la",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Latvia",
        "cc": "+371",
        "ic": "lv",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Lebanon",
        "cc": "+961",
        "ic": "lb",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Lesotho",
        "cc": "+266",
        "ic": "ls",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Liberia",
        "cc": "+231",
        "ic": "lr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Libya",
        "cc": "+218",
        "ic": "ly",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Liechtenstein",
        "cc": "+423",
        "ic": "li",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Lithuania",
        "cc": "+370",
        "ic": "lt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Luxembourg",
        "cc": "+352",
        "ic": "lu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Macau",
        "cc": "+853",
        "ic": "mo",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Macedonia",
        "cc": "+389",
        "ic": "mk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Madagascar",
        "cc": "+261",
        "ic": "mg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Malawi",
        "cc": "+265",
        "ic": "mw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Malaysia",
        "cc": "+60",
        "ic": "my",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Maldives",
        "cc": "+960",
        "ic": "mv",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mali",
        "cc": "+223",
        "ic": "ml",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Malta",
        "cc": "+356",
        "ic": "mt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Marshall Islands",
        "cc": "+692",
        "ic": "mh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Martinique",
        "cc": "+596",
        "ic": "mq",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mauritania",
        "cc": "+222",
        "ic": "mr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mauritius",
        "cc": "+230",
        "ic": "mu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mayotte",
        "cc": "+262",
        "ic": "yt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mexico",
        "cc": "+52",
        "ic": "mx",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Micronesia",
        "cc": "+691",
        "ic": "fm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Moldova",
        "cc": "+373",
        "ic": "md",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Monaco",
        "cc": "+377",
        "ic": "mc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mongolia",
        "cc": "+976",
        "ic": "mn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Montenegro",
        "cc": "+382",
        "ic": "me",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Montserrat",
        "cc": "+1-664",
        "ic": "ms",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Morocco",
        "cc": "+212",
        "ic": "ma",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Mozambique",
        "cc": "+258",
        "ic": "mz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Myanmar",
        "cc": "+95",
        "ic": "mm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Namibia",
        "cc": "+264",
        "ic": "na",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Nauru",
        "cc": "+674",
        "ic": "nr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Nepal",
        "cc": "+977",
        "ic": "np",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Netherlands",
        "cc": "+31",
        "ic": "nl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "New Caledonia",
        "cc": "+687",
        "ic": "nc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "New Zealand",
        "cc": "+64",
        "ic": "nz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Nicaragua",
        "cc": "+505",
        "ic": "ni",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Niger",
        "cc": "+227",
        "ic": "ne",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Nigeria",
        "cc": "+234",
        "ic": "ng",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Niue",
        "cc": "+683",
        "ic": "nu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Norfolk Island",
        "cc": "+672",
        "ic": "nf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "North Korea",
        "cc": "+850",
        "ic": "kp",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Northern Mariana Islands",
        "cc": "+1-670",
        "ic": "mp",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Norway",
        "cc": "+47",
        "ic": "no",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Oman",
        "cc": "+968",
        "ic": "om",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Pakistan",
        "cc": "+92",
        "ic": "pk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Palau",
        "cc": "+680",
        "ic": "pw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Palestine",
        "cc": "+970",
        "ic": "ps",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Panama",
        "cc": "+507",
        "ic": "pa",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Papua New Guinea",
        "cc": "+675",
        "ic": "pg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Paraguay",
        "cc": "+595",
        "ic": "py",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Peru",
        "cc": "+51",
        "ic": "pe",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Philippines",
        "cc": "+63",
        "ic": "ph",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Pitcairn Islands",
        "cc": "+64",
        "ic": "pn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Poland",
        "cc": "+48",
        "ic": "pl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Portugal",
        "cc": "+351",
        "ic": "pt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Puerto Rico",
        "cc": "+1",
        "ic": "pr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Qatar",
        "cc": "+974",
        "ic": "qa",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Republic of the Congo",
        "cc": "+242",
        "ic": "cg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Romania",
        "cc": "+40",
        "ic": "ro",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Russia",
        "cc": "+7",
        "ic": "ru",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Rwanda",
        "cc": "+250",
        "ic": "rw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Barthélemy",
        "cc": "+590",
        "ic": "bl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Helena",
        "cc": "+290",
        "ic": "sh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Kitts and Nevis",
        "cc": "+1-869",
        "ic": "kn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Lucia",
        "cc": "+1-758",
        "ic": "lc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Martin",
        "cc": "+590",
        "ic": "mf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saint Vincent and the Grenadines",
        "cc": "+1-784",
        "ic": "vc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Samoa",
        "cc": "+1-685",
        "ic": "ws",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "San Marino",
        "cc": "+378",
        "ic": "sm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Saudi Arabia",
        "cc": "+966",
        "ic": "sa",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Senegal",
        "cc": "+221",
        "ic": "sn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Serbia",
        "cc": "+381",
        "ic": "rs",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Seychelles",
        "cc": "+248",
        "ic": "sc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Sierra Leone",
        "cc": "+234",
        "ic": "sl",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Singapore",
        "cc": "+65",
        "ic": "sg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Slovakia",
        "cc": "+421",
        "ic": "sk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Slovenia",
        "cc": "+386",
        "ic": "si",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Solomon Islands",
        "cc": "+677",
        "ic": "sb",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Somalia",
        "cc": "+252",
        "ic": "so",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "South Africa",
        "cc": "+27",
        "ic": "za",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "South Korea",
        "cc": "+82",
        "ic": "kr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "South Sudan",
        "cc": "+211",
        "ic": "ss",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Spain",
        "cc": "+34",
        "ic": "es",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Sri Lanka",
        "cc": "+94",
        "ic": "lk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Sudan",
        "cc": "+249",
        "ic": "sd",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Suriname",
        "cc": "+597",
        "ic": "sr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Swaziland",
        "cc": "+268",
        "ic": "sz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Sweden",
        "cc": "+46",
        "ic": "se",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Switzerland",
        "cc": "+41",
        "ic": "ch",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Syria",
        "cc": "+963",
        "ic": "sy",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Taiwan",
        "cc": "+886",
        "ic": "tw",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tajikistan",
        "cc": "+992",
        "ic": "tj",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tanzania",
        "cc": "+255",
        "ic": "tz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Thailand",
        "cc": "+66",
        "ic": "th",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Togo",
        "cc": "+228",
        "ic": "tg",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tokelau",
        "cc": "+690",
        "ic": "tk",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tonga",
        "cc": "+676",
        "ic": "to",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Trinidad and Tobago",
        "cc": "+1-868",
        "ic": "tt",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tunisia",
        "cc": "+216",
        "ic": "tn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Turkey",
        "cc": "+90",
        "ic": "tr",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Turkmenistan",
        "cc": "+993",
        "ic": "tm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Turks and Caicos Islands",
        "cc": "+1-649",
        "ic": "tc",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Tuvalu",
        "cc": "+688",
        "ic": "tv",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Uganda",
        "cc": "+256",
        "ic": "ug",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Ukraine",
        "cc": "+380",
        "ic": "ua",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "United Arab Emirates",
        "cc": "+971",
        "ic": "ae",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "United Kingdom",
        "cc": "+44",
        "ic": "gb",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "United States",
        "cc": "+1",
        "ic": "us",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "United States Virgin Islands",
        "cc": "+1-340",
        "ic": "vi",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Uruguay",
        "cc": "+598",
        "ic": "uy",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Uzbekistan",
        "cc": "+998",
        "ic": "uz",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Vanuatu",
        "cc": "+678",
        "ic": "vu",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Vatican",
        "cc": "+379",
        "ic": "va",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Venezuela",
        "cc": "+58",
        "ic": "ve",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Vietnam",
        "cc": "+84",
        "ic": "vn",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Wallis and Futuna",
        "cc": "+681",
        "ic": "wf",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Western Sahara",
        "cc": "+212",
        "ic": "eh",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Yemen",
        "cc": "+967",
        "ic": "ye",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Zambia",
        "cc": "+260",
        "ic": "zm",
        "cdt": new Date(),
        "ldt": new Date()
    },
    {
        "n": "Zimbabwe",
        "cc": "+263",
        "ic": "zw",
        "cdt": new Date(),
        "ldt": new Date()
    }
];

_db = db.getSiblingDB('traviard_secdb');
_db.auth(__suser.uname, __suser.pass);
_db.country.insert(countries);
