# Traviard Cloud - Development

#### TODO Items (Postponed)

- Ribbon (Client-side load-balancing) thorough configuration.
- Eureka one configurations.
- Docker automation scripting (Add logstash filter).
- Take care of indexing and visualization in Kibana. 
- Handle the AccessDenied in Spring Security - ```cloud-seurity``` module developments.
- Swagger documentation work started under ```feature/trace-logger``` branch and just briefly implemented in ```logger-service``` module. Need to implement on all of the 
modules and centralize the documentation access. (Code dependency put in ```traviard-cloud``` module and ```-ui``` dependency only put in ```logger-service``` module)
- Tune-up REST endpoints on ```com.traviard.cloud.security.controller.UserController``` class to return at least empty `String` to comply with HTTP standards.

##### Remember!

- Update the MDBootstrap License before public deployment.

#### TODO items before starts with the AWS client implementations for DataStore

1. ~~Move all AWS client dependencies to `aws-clients` module from `reactorcommon-lib`.~~
2. ~~Update `com.traviard.cloud.reactor.config.WebRoutingConfig` to resolve "contextPath" itself instead of define the variable on each of the module and inject it into this class.~~
3. ~~Put `@author` comment on `com.traviard.cloud.aws.config.AwsClientAccessConfig` class.~~
4. ~~Remove `spring-boot-starter-webflux` dependency from `webservice-lib` module. (Resolve as a transitive dependency)~~
5. ~~Remove the `provided` scope from `spring-boot-starter-webflux` dependency in `cloud-libs` module. (Cannot remove - Need to resolve compile time)~~
6. ~~Check all the explicitly defined dependency versions and move those version declarations to BOM module.~~
7. Before `cloud-datastoreweb` or `cloud-aws-clients` modules, implement a way to secure all other services through `cloud-security` service.