package com.traviard.cloud.logger.controller;

import com.traviard.cloud.log4j.util.Message;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @author Sachith Dickwella
 */
@RestController
public class LoggerController {

    /**
     * {@link KafkaTemplate} instance to send messages.
     */
    private final KafkaTemplate<Integer, Message> template;

    /**
     * Constructor to {@link Autowired} required beans.
     *
     * @param template instance of {@link KafkaTemplate}.
     */
    @Autowired
    public LoggerController(KafkaTemplate<Integer, Message> template) {
        this.template = template;
    }

    /**
     * Request mapping function to do the controller task. Particularly,
     * {@link PostMapping} here.
     *
     * @param message to send for message broker.
     * @return an instance of {@link Mono} with {@link Void}.
     */
    @PostMapping(path = "/log", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Void>> log(@NotNull @RequestBody Message message) {
        template.send("topicName", message);
        return Mono.just(ResponseEntity.ok().build());
    }
}
