package com.traviard.cloud.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @author Sachith Dickwella
 */
@EnableEurekaClient
@EnableWebFlux
@SpringBootApplication
public class AsyncLoggerApp {

    public static void main(String[] args) {
        SpringApplication.run(AsyncLoggerApp.class, args);
    }
}
