package com.traviard.cloud.logger.config;

import com.traviard.cloud.kafka.KafkaAdminConfig;
import com.traviard.cloud.kafka.util.CompressionType;
import com.traviard.cloud.log4j.util.Message;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sachith Dickwella
 */
@Configuration
@PropertySource("classpath:kafka-config.properties")
public class KafkaClientConfig implements KafkaAdminConfig {

    /**
     * Logger Kafka topic name.
     */
    @Value("${traviard.kafka-broker.config.topic.1}")
    private String topicName;
    /**
     * Number partitions per broker instance.
     */
    @Value("#{T(Integer).parseInt('${traviard.kafka-broker.config.topic.1.partitions}')}")
    private int partitions;
    /**
     * Number of replications available to the cluster.
     */
    @Value("#{T(Integer).parseInt('${traviard.kafka-broker.config.topic.1.replications}')}")
    private int replications;
    /**
     * Message compression type.
     */
    @Value("#{T(com.traviard.cloud.kafka.util.CompressionType).of('${traviard.kafka-broker.config.topic.1.compression-type}')}")
    private CompressionType compressionType;
    /**
     * Admin configuration {@link Map} to kafka client.
     */
    @Value("#{${traviard.kafka-broker.configs}}")
    private Map<String, Object> configs;

    /**
     * Kafka topic to enqueue the logging messages.
     */
    @Bean("loggerTopic")
    public NewTopic getLoggerTopic() {
        return TopicBuilder.name(topicName)
                .partitions(partitions)
                .replicas(replications)
                .config(TopicConfig.COMPRESSION_TYPE_CONFIG, compressionType.type())
                .build();
    }

    /**
     * Create and register new {@link KafkaTemplate} instance as Spring
     * bean. Set the {@link #topicName} as the default topic.
     *
     * @param producerFactory inject {@link ProducerFactory} instance.
     * @return new {@link KafkaTemplate} instance.
     */
    @Bean
    public KafkaTemplate<Integer, Message> getKafkaTemplate(ProducerFactory<Integer, Message> producerFactory) {
        final var template = new KafkaTemplate<>(producerFactory);
        template.setDefaultTopic(topicName);

        return template;
    }

    /**
     * New instance of {@link ProducerFactory} bean registration.
     *
     * @return new instance of {@link DefaultKafkaProducerFactory}.
     */
    @Bean
    public ProducerFactory<Integer, Message> getDefaultProducerFactory() {
        return new DefaultKafkaProducerFactory<>(configs());
    }

    /**
     * Get configuration for Kafka broker from the downstream
     * definitions.
     *
     * These configurations inject from the {@code kafka-config.properties}
     * file are originally declared in {@link org.apache.kafka.clients.admin.AdminClientConfig}
     * class and/or {@link org.apache.kafka.clients.CommonClientConfigs} class.
     *
     * More configs at :
     * <link>
     * https://kafka.apache.org/documentation/#producerconfigs
     * </link>
     *
     * @return a {@link Map} of {@link String} keys and {@link Object} values.
     */
    @Override
    public Map<String, Object> configs() {
        final var newConfigs = new HashMap<>(configs);
        newConfigs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        newConfigs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        return newConfigs;
    }
}
