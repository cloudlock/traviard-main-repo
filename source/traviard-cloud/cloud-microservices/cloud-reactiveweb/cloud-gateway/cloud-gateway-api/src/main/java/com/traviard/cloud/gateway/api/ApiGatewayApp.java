package com.traviard.cloud.gateway.api;

import com.traviard.cloud.log4j.Logger;
import com.traviard.cloud.log4j.logger.ServiceLogger;
import com.traviard.cloud.log4j.util.Type;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author Sachith Dickwella
 */
@EnableEurekaClient
@EnableAutoConfiguration
@ComponentScan({
        "com.traviard.cloud.security",
        "com.traviard.cloud.gateway.filters",
        "com.traviard.cloud.gateway.filters.config"
})
public class ApiGatewayApp {

    /**
     * Application name (AutoWired).
     */
    @Value("${spring.application.name}")
    private String serviceName;

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApp.class, args);
    }

    @Bean("serviceName")
    public String getServiceName() {
        return serviceName;
    }

    @LoadBalanced
    @Bean
    public WebClient getWebClient(LoadBalancerClient loadBalancerClient) {
        return WebClient.builder()
                .filter(new LoadBalancerExchangeFilterFunction(loadBalancerClient))
                .build();
    }

    @Bean
    public Logger getLogger(WebClient client, String serviceName) {
        return new ServiceLogger(client, serviceName, Type.SERVICE);
    }
}
