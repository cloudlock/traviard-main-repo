package com.traviard.cloud.gateway.filters.util;

import com.traviard.cloud.log4j.Logger;
import com.traviard.cloud.security.Config;
import com.traviard.cloud.security.util.Token;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.*;
import java.util.regex.Pattern;

import static com.traviard.cloud.security.SecurityConstants.COOKIE_MAX_AGE;
import static com.traviard.cloud.security.SecurityConstants.Cookies.SECURITY_TOKEN;
import static com.traviard.cloud.security.SecurityConstants.Properties;
import static com.traviard.cloud.util.ApplicationConstants.Headers.X_APPLICATION_ID;
import static com.traviard.cloud.util.ApplicationConstants.QueryParamNames.*;

/**
 * @author Sachith Dickwella
 */
@Component
public class SecurityServiceClient {

    /**
     * Reference query parameter capture pattern.
     */
    private static final Pattern REF_QUERY_PATTERN = Pattern.compile("ref(=([^&#]*)|&|#|$)[^&#]");
    /**
     * Security service client instance.
     */
    private final WebClient client;
    /**
     * {@link ResourceBundle} to access properties.
     */
    private final ResourceBundle bundle;
    /**
     * Instance of utility {@link Config} class.
     */
    private final Config config;
    /**
     * WebFlux logging instance.
     */
    private final Logger logger;

    /**
     * Autowired args constructor.
     *
     * @param client WebMvc {@link WebClient} instance
     * @param config Instance of {@link Config}
     * @param logger instance of {@link Logger}.
     */
    @Autowired
    public SecurityServiceClient(WebClient client, Config config, Logger logger) {
        this.client = client;
        this.config = config;
        this.bundle = ResourceBundle.getBundle(Properties.FILE_SECURITY_CONFIG, Locale.US);
        this.logger = logger;
    }

    /**
     * Client implementation to validate token.
     *
     * @param tokenCookie {@link HttpCookie} token
     * @return the status of the token validation {@link Mono}
     */
    @NotNull
    public Mono<Void> isValidToken(@NotNull ServerWebExchange exchange, @NotNull GatewayFilterChain chain,
                                   @NotNull HttpCookie tokenCookie) {
        return client.post()
                .uri(URI.create(bundle.getString(Properties.PROPERTY_SECURITY_SERVICE_VALIDATE_TOKEN_URL)))
                .headers(e -> e.addAll(config.validHeaders(tokenCookie)))
                .exchange()
                .flatMap(cr -> {
                    if (cr.statusCode().equals(HttpStatus.OK)) return chain.filter(exchange);
                    else return getUnauthorizedResponse(exchange, chain);
                });
    }

    /**
     * Client implementation of the refresh token.
     *
     * @param exchange    {@link ServerWebExchange} instance from upstream
     * @param tokenCookie {@link HttpCookie} token
     * @return the refreshed token value {@link Mono}
     */
    @NotNull
    public Mono<Void> refreshToken(@NotNull ServerWebExchange exchange, @NotNull GatewayFilterChain chain,
                                   @NotNull HttpCookie tokenCookie) {
        return client.post()
                .uri(URI.create(bundle.getString(Properties.PROPERTY_SECURITY_SERVICE_REFRESH_TOKEN_URL)))
                .headers(e -> e.addAll(config.validHeaders(tokenCookie)))
                .retrieve()
                .bodyToMono(Token.class)
                .flatMap(token -> {
                    var response = exchange.getResponse();
                    ResponseCookie newCookie;
                    if (token != null && StringUtils.isNotEmpty(token.getValue())) {
                        newCookie = ResponseCookie
                                .from(SECURITY_TOKEN.getName(), token.getValue())
                                .path(SECURITY_TOKEN.getPath())
                                .httpOnly(true)
                                .maxAge(Duration.ofMinutes(COOKIE_MAX_AGE))
                                .build();
                    } else {
                        newCookie = ResponseCookie
                                .from(SECURITY_TOKEN.getName(), StringUtils.EMPTY)
                                .path(SECURITY_TOKEN.getPath())
                                .httpOnly(true)
                                .maxAge(0)
                                .build();
                    }

                    response.addCookie(newCookie);
                    return chain.filter(exchange.mutate().response(response).build());
                });
    }

    /**
     * Return mutated {@link Mono<Void>} instance with the response based on the token cookie
     * availability and authenticity of the cookie value.
     *
     * @param exchange Instance of {@link ServerWebExchange}
     * @param chain    Instance of {@link GatewayFilterChain}
     * @return Instance of {@link Mono<Void>} publisher type
     */
    @NotNull
    public Mono<Void> getUnauthorizedResponse(@NotNull ServerWebExchange exchange,
                                              @NotNull GatewayFilterChain chain) {
        final var response = exchange.getResponse();

        response.getHeaders().setLocation(getAuthURL(exchange.getRequest().getURI(), prepQuery(exchange)));
        return chain.filter(exchange.mutate().response(response).build())
                .then(Mono.fromRunnable(() -> response.setStatusCode(HttpStatus.FOUND)));
    }

    /**
     * Orchestrate the redirect path which failed for.
     *
     * @param exchange Instance of {@link ServerWebExchange}
     * @return new query parameter with failed path
     */
    @NotNull
    private String prepQuery(@NotNull final ServerWebExchange exchange) {
        final var request = exchange.getRequest();
        final var params = new ArrayList<String>();

        // Existing application web query parameters.
        params.add(request.getURI().getQuery());
        // Application id query parameter.
        params.add(Optional.ofNullable(exchange.getResponse().getHeaders().get(X_APPLICATION_ID.getName()))
                .stream()
                .flatMap(Collection::stream)
                .filter(StringUtils::isNotEmpty)
                .map(e -> appendParamValue(APPLICATION_ID_PARAM, e))
                .findFirst()
                .orElse(StringUtils.EMPTY));
        // Referer header value, reference query parameter.
        params.add(Optional.ofNullable(request.getHeaders().get(HttpHeaders.REFERER))
                .stream()
                .flatMap(Collection::stream)
                .filter(StringUtils::isNotEmpty)
                .map(StringBuffer::new)
                .map(sb -> {
                    var m = REF_QUERY_PATTERN.matcher(sb);
                    if (m.find()) {
                        sb.append(encodeUTF8(m.group(2)));
                    }
                    return appendParamValue(REFERER_HEADER_PARAM, encodeUTF8(sb));
                })
                .findFirst()
                .orElse(StringUtils.EMPTY));

        return appendQueryParams(params);
    }

    /**
     * Generate auth-service {@link URI} object from upstream request.
     *
     * @param uri   Upstream request's {@link URI} instance
     * @param query Custom orchestrated query parameter {@link String}s
     * @return provisioned new {@link URI} object pointed to auth-service
     */
    @Nullable
    private URI getAuthURL(@NotNull URI uri, @Nullable String query) {
        try {
            return new URI(uri.getScheme(),
                    uri.getUserInfo(),
                    uri.getHost(),
                    uri.getPort(),
                    "/auth",
                    query,
                    uri.getFragment());
        } catch (URISyntaxException ex) {
            /*
             * This would never occur at runtime if the above details provided for the URI
             * builder are correct.
             */
            logger.error(() -> "URI syntax error", () -> ex);
        }
        return null;
    }

    /**
     * Encode the URL parameter value to comply with HTTP standard with UTF-8
     * charset.
     *
     * @param url {@link T} type parameter {@code toString()}.
     * @return HTTP encoded parameter value.
     */
    @NotNull
    private <T> String encodeUTF8(@NotNull T url) {
        return URLEncoder.encode(url.toString(), StandardCharsets.UTF_8);
    }
}
