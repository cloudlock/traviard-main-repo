package com.traviard.cloud.gateway.filters;

import com.traviard.cloud.gateway.filters.config.HttpEndpointSecurityConfig;
import com.traviard.cloud.gateway.filters.util.SecurityServiceClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static com.traviard.cloud.security.SecurityConstants.Cookies;

/**
 * This particular filter use to check whether certain set of URLs configured in
 * property files, can be exempt from cookie validation and if not do the validation
 * with {@code security-service}.
 *
 * @author Sachith Dickwella
 */
@Component
public class AuthRoutingPreFilter extends AbstractNameValueGatewayFilterFactory implements Ordered {

    /**
     * Allowed and denied configuration settings.
     */
    private HttpEndpointSecurityConfig securityConfig;
    /**
     * Client for the security-service.
     */
    private SecurityServiceClient config;

    /**
     * Autowired args constructor.
     *
     * @param config         Instance of {@link SecurityServiceClient}
     * @param securityConfig Instance of {@link HttpEndpointSecurityConfig}
     */
    @Autowired
    public AuthRoutingPreFilter(SecurityServiceClient config,
                                HttpEndpointSecurityConfig securityConfig) {
        this.config = config;
        this.securityConfig = securityConfig;
    }

    /**
     * Get the {@link GatewayFilter} instance to enforce filter.
     *
     * @param config Configuration {@link NameValueConfig} instance
     * @return instance of {@link GatewayFilter}
     */
    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
            final var request = exchange.getRequest();
            if (securityConfig.getAllowedUrls().get(config.getName())
                    .stream()
                    .filter(StringUtils::isNotEmpty)
                    .anyMatch(url -> {
                        var requestUrl = request.getPath().toString();
                        if (url.matches("^/.*/\\*{2}$")) {
                            return requestUrl.startsWith(url.replaceFirst("/\\*{2}", ""));
                        }
                        return url.equalsIgnoreCase(requestUrl);
                    })) {
                return chain.filter(exchange);
            } else {
                var cookies = request.getCookies();
                if (cookies.containsKey(Cookies.SECURITY_TOKEN.getName())) {
                    return this.config
                            .isValidToken(exchange, chain,
                                    Objects.requireNonNull(cookies.getFirst(Cookies.SECURITY_TOKEN.getName())));
                }
            }
            /*
             * Mutated {@code Mono<Void>} instance.
             */
            return this.config.getUnauthorizedResponse(exchange, chain);
        };
    }

    /**
     * Get the order value of this object.
     * <p>Higher values are interpreted as lower priority. As a consequence,
     * the object with the lowest value has the highest priority (somewhat
     * analogous to Servlet {@code load-on-startup} values).
     * <p>Same order values will result in arbitrary sort positions for the
     * affected objects.
     *
     * @return the order value
     * @see #HIGHEST_PRECEDENCE
     * @see #LOWEST_PRECEDENCE
     */
    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE + 10;
    }
}
