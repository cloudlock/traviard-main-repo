package com.traviard.cloud.gateway.filters.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sachith Dickwella
 */
@Scope("singleton")
@Component("securityConfig")
public class SecurityConfigMapper {

    /**
     * Decode and wrap {@code Accept} header values into a {@link Set}.
     *
     * @param mimeList Comma separated header values
     * @return decoded comma separated {@link String} into a {@link Set}
     */
    @NotNull
    public Set<MediaType> mediaTypes(@NotNull String mimeList) {
        return Stream.of(mimeList.split(",")).map(MediaType::valueOf).collect(Collectors.toSet());
    }

    /**
     * Decode and map the input {@link String} to a {@code Map} with allowable URL
     * endpoints. {@link String} input decodes by {@code '(\w+)(=)(\[(.*?)])'} regular
     * expression.
     *
     * @param urlMap {@link String} mapped-url structure
     * @return {@link Map} of service oriented allowable URL {@link Set}
     */
    @NotNull
    public Map<String, Set<String>> urls(@NotNull String urlMap) {
        Pattern pattern = Pattern.compile("(\\w+)(=)(\\[(.*?)])");
        Matcher matcher = pattern.matcher(urlMap);

        Map<String, Set<String>> urls = new HashMap<>();
        while (matcher.find()) {
            urls.put(matcher.group(1), Stream.of(matcher.group(4).split(",")).collect(Collectors.toSet()));
        }
        return urls;
    }
}
