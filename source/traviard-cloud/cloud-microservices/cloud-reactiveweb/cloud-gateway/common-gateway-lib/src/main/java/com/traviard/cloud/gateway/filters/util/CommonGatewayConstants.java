package com.traviard.cloud.gateway.filters.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Sachith Dickwella
 */
public interface CommonGatewayConstants {

    @Getter
    @AllArgsConstructor
    enum Messages {
        /**
         * IO exception handling message.
         */
        MESSAGE_IO_EXCEPTION("I/O exception occurred"),
        /**
         * Content-Type header not found.
         */
        MESSAGE_NO_CONTENT_TYPE("Content-Type header not available");

        /**
         * String message instance to hold the actual message.
         */
        private String message;
    }
}
