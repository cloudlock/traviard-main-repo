package com.traviard.cloud.gateway.filters;

import com.traviard.cloud.gateway.filters.config.HttpEndpointSecurityConfig;
import com.traviard.cloud.gateway.filters.util.SecurityServiceClient;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static com.traviard.cloud.security.SecurityConstants.Cookies.SECURITY_TOKEN;

/**
 * This filter is used to refresh/reset the {@code TOKEN} cookie's {@code maxAge}
 * attribute on every http request particular to {@code text/html} header {@code ContentType}
 * requests.
 * <p>
 * With this filter, application will be able to maintain a active session respective to the
 * user's activity, otherwise {@code TOKEN} will be deleted after the given time when it's created
 * even when use's active on the application.
 *
 * @author Sachith Dickwella
 */
@Component
public class AuthRoutingCookieRefreshPostFilter extends AbstractGatewayFilterFactory<Void> {

    /**
     * Allowed and denied configuration settings.
     */
    private HttpEndpointSecurityConfig securityConfig;
    /**
     * Client for the security-service.
     */
    private SecurityServiceClient config;

    /**
     * Double-args constructor to autowire {@link HttpEndpointSecurityConfig} instance.
     *
     * @param config         Instance of {@link SecurityServiceClient}
     * @param securityConfig Instance of {@link HttpEndpointSecurityConfig}
     */
    @Autowired
    public AuthRoutingCookieRefreshPostFilter(SecurityServiceClient config,
                                              HttpEndpointSecurityConfig securityConfig) {
        super(Void.class);
        this.securityConfig = securityConfig;
        this.config = config;
    }

    /**
     * Get the {@link GatewayFilter} instance to enforce filter.
     *
     * @return instance of {@link GatewayFilter}
     */
    @Override
    public GatewayFilter apply(Void config) {
        return new CookieRefreshFilter();
    }

    /**
     * Refresh the token cookie's {@code maxAge} when the filter hits.
     *
     * @see GatewayFilter
     * @see Ordered
     */
    private class CookieRefreshFilter implements GatewayFilter, Ordered {

        /**
         * Process the Web request and (optionally) delegate to the next
         * {@code WebFilter} through the given {@link GatewayFilterChain}.
         *
         * @param exchange the current server exchange
         * @param chain    provides a way to delegate to the next filter
         * @return {@code Mono<Void>} to indicate when request processing is complete
         */
        @Override
        public Mono<Void> filter(@NotNull ServerWebExchange exchange, @NotNull GatewayFilterChain chain) {
            final ServerHttpRequest request = exchange.getRequest();
            return request.getHeaders().getAccept()
                    .stream()
                    .filter(header -> securityConfig.getDeniedHeaders().stream().noneMatch(header::includes)
                            && securityConfig.getAllowedHeaders().stream().anyMatch(header::includes))
                    .findAny()
                    .flatMap(l -> Optional.ofNullable(request.getCookies().get(SECURITY_TOKEN.getName())))
                    .flatMap(l -> l.stream().findAny())
                    .map(cookie -> config.refreshToken(exchange, chain, cookie))
                    .orElseGet(() -> chain.filter(exchange));
        }

        /**
         * Get the order value of this object.
         * <p>Higher values are interpreted as lower priority. As a consequence,
         * the object with the lowest value has the highest priority (somewhat
         * analogous to Servlet {@code load-on-startup} values).
         * <p>Same order values will result in arbitrary sort positions for the
         * affected objects.
         *
         * @return the order value
         * @see #HIGHEST_PRECEDENCE
         * @see #LOWEST_PRECEDENCE
         */
        @Override
        public int getOrder() {
            return Ordered.LOWEST_PRECEDENCE;
        }
    }
}
