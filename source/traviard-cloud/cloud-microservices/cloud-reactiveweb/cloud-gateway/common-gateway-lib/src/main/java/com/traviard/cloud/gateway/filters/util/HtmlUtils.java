package com.traviard.cloud.gateway.filters.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

/**
 * HTML content related utility includes here.
 */
public interface HtmlUtils {

    /**
     * HTML element details and important attribute mappings.
     */
    @Getter
    @AllArgsConstructor
    enum HtmlElement {
        /**
         * Html {@code <script>} tag name attribute name.
         */
        SCRIPT("script", "src"),
        /**
         * Html {@code <link>} tag name and attribute name.
         */
        LINK("link", "href"),
        /**
         * Html {@code <img>} tag name and attribute name.
         */
        IMG("img", "src"),
        /**
         * Html {@code <form>} tag name and attribute name.
         */
        FORM("form", "action");

        /**
         * Html element name.
         */
        private String element;
        /**
         * Html element's attribute name which need to update.
         */
        private String attribute;
    }

    /**
     * Represent the HTML document model.
     */
    @Setter
    @Getter
    class Dom {
        /**
         * String representation of HTML doc.
         */
        private String html;

        /**
         * Private single-arg constructor to initialize instance variables.
         */
        private Dom(String html) {
            this.html = html;
        }

        /**
         * Get fresh instance of {@link Dom} class.
         *
         * @param html Original html content from the requested page.
         * @return new instance of {@link Dom} class
         */
        @NotNull
        public static Dom getInstance(@NotNull String html) {
            return new Dom(html);
        }
    }

    /**
     * Get the pattern instance to match HTML element.
     *
     * @param elements Instance of {@link HtmlElement}
     * @return a instance of {@link java.util.regex.Pattern}
     */
    @NotNull
    static Pattern regex(@NotNull HtmlElement elements) {
        return Pattern.compile(String.format("<%s.*%s=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"'].*>?",
                elements.getElement(), elements.getAttribute()));
    }
}
