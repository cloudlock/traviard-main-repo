package com.traviard.cloud.gateway.filters.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

/**
 * @author Sachith Dickwella
 */
@Getter
@Scope("singleton")
@Component
@RefreshScope
@PropertySource("classpath:http-security-config.properties")
public class HttpEndpointSecurityConfig {

    /**
     * {@link Set} of {@link String} allowable URL endpoints.
     */
    @Value("#{securityConfig.urls('${me.auth.allowed.urls}')}")
    private Map<String, Set<String>> allowedUrls;
    /**
     * {@link Set} of {@link MediaType} cookie refresh allowable headers.
     */
    @Value("#{securityConfig.mediaTypes('${me.auth.cookie.refresh.allowed.headers}')}")
    private Set<MediaType> allowedHeaders;
    /**
     * {@link Set} of {@link MediaType} cookie refresh deniable headers.
     */
    @Value("#{securityConfig.mediaTypes('${me.auth.cookie.refresh.denied.headers}')}")
    private Set<MediaType> deniedHeaders;
}