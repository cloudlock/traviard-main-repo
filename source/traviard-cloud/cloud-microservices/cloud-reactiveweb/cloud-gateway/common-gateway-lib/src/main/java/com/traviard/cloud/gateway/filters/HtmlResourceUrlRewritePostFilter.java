package com.traviard.cloud.gateway.filters;

import com.traviard.cloud.gateway.filters.config.FilterClientResponse;
import com.traviard.cloud.gateway.filters.util.GatewayFilterException;
import com.traviard.cloud.gateway.filters.util.HtmlUtils;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.traviard.cloud.gateway.filters.util.CommonGatewayConstants.Messages.MESSAGE_IO_EXCEPTION;
import static com.traviard.cloud.gateway.filters.util.CommonGatewayConstants.Messages.MESSAGE_NO_CONTENT_TYPE;
import static com.traviard.cloud.gateway.filters.util.HtmlUtils.HtmlElement;
import static com.traviard.cloud.util.StringUtils.LF;

/**
 * @author Sachith Dickwella
 */
@Component
public class HtmlResourceUrlRewritePostFilter
        extends AbstractGatewayFilterFactory<HtmlResourceUrlRewritePostFilter.Config> {

    /**
     * Filter parameter key {@code prefixCount}.
     */
    private static final String PREFIX_COUNT_KEY = "prefixCount";

    @Autowired
    public HtmlResourceUrlRewritePostFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return new ReplaceUrlsFilter(config);
    }

    /**
     * {@link GatewayFilter} implementation to rewrite response body. Also implements the {@link Ordered}
     * interface to define filter's location order of the chain.
     */
    private static class ReplaceUrlsFilter implements GatewayFilter, Ordered {

        private Config config;

        private ReplaceUrlsFilter(Config config) {
            this.config = config;
        }

        @Override
        public Mono<Void> filter(@NotNull ServerWebExchange exchange, @NotNull GatewayFilterChain chain) {
            final var response = exchange.getResponse();
            final var request = exchange.getRequest();
            return chain.filter(exchange.mutate().response(getServerHttpResponseDecorator(request, response)).build())
                    .then(Mono.fromRunnable(() -> response.setStatusCode(HttpStatus.OK)));
        }

        /**
         * Get the order value of this object.
         *
         * <p>Higher values are interpreted as lower priority. As a consequence,
         * the object with the lowest value has the highest priority (somewhat
         * analogous to Servlet {@code load-on-startup} values).
         * <p>Same order values will result in arbitrary sort positions for the
         * affected objects.
         *
         * @return the order value
         */
        @Override
        public int getOrder() {
            return NettyWriteResponseFilter.WRITE_RESPONSE_FILTER_ORDER - 1;
        }

        /**
         * Replace provided element's resource url values with Zuul proxy's context path.
         *
         * @param dom         HTML web page's {@link HtmlUtils.Dom} representation
         * @param request     {@link ServerHttpRequest} instance
         * @param prefixCount PrefixCount parameter for the filter
         * @return modified {@link HtmlUtils.Dom} instance
         */
        private HtmlUtils.Dom replaceURL(final HtmlUtils.Dom dom, ServerHttpRequest request, int prefixCount) {
            if (prefixCount > 0) {
                final var strippedPath = request.getPath().toString().split("/")[prefixCount];
                Stream.of(HtmlElement.values())
                        .map(HtmlUtils::regex)
                        .map(p -> p.matcher(dom.getHtml()))
                        .forEach(m -> {
                            while (m.find()) {
                                dom.setHtml(StringUtils.replace(
                                        dom.getHtml(),
                                        String.format("\"%s\"", m.group(1)),
                                        String.format("\"%s/%s\"", strippedPath, m.group(1))));
                            }
                        });
            }
            return dom;
        }

        /**
         * Modify the response body. Particularly, modify the resource URL context paths for {@code <script>},
         * {@code <link>}, {@code <img>} and {@code <form>} elements. More elements could be added in the future. If so,
         * no change is required here hence the process takes all the declared enum values from {@link HtmlUtils}
         * enum in the {@code replaceURL} function.
         *
         * @param request  Instance of {@link ServerHttpRequest} from actual request
         * @param response Instance of {@link ServerHttpResponse} for the actual response
         * @return an instance of {@link ServerHttpResponseDecorator} class
         */
        @NotNull
        private ServerHttpResponseDecorator getServerHttpResponseDecorator(@NotNull ServerHttpRequest request,
                                                                           @NotNull ServerHttpResponse response) {
            final var dbf = response.bufferFactory();
            return new ServerHttpResponseDecorator(response) {

                @NotNull
                @Override
                public Mono<Void> writeWith(@NotNull Publisher<? extends DataBuffer> body) {
                    final var flux = FilterClientResponse.getInstance(body, response.getHeaders()).getBody();
                    return super.writeWith(flux.map(df -> {
                        if (Objects.requireNonNull(response.getHeaders().getContentType(), MESSAGE_NO_CONTENT_TYPE.getMessage())
                                .includes(MediaType.TEXT_HTML)) {

                            try (BufferedReader br = new BufferedReader(new InputStreamReader(df.asInputStream()))) {
                                var html = br.lines().collect(Collectors.joining(LF));
                                return dbf.wrap(
                                        replaceURL(HtmlUtils.Dom.getInstance(html), request, config.prefixCount)
                                                .getHtml()
                                                .getBytes(StandardCharsets.UTF_8));
                            } catch (IOException ex) {
                                throw new GatewayFilterException(MESSAGE_IO_EXCEPTION.getMessage(), ex);
                            }
                        }
                        return dbf.wrap(df.asByteBuffer());
                    }));
                }
            };
        }
    }

    /**
     * Returns hints about the number of args and the order for shortcut parsing.
     *
     * @return return argument list
     */
    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList(PREFIX_COUNT_KEY);
    }

    /**
     * Configuration container class.
     */
    public static class Config {

        private int prefixCount;

        @SuppressWarnings("unused")
        public void setPrefixCount(int prefixCount) {
            this.prefixCount = prefixCount;
        }
    }
}