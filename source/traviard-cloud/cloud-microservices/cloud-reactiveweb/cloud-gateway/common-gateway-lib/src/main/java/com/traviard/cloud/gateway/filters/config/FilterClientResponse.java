package com.traviard.cloud.gateway.filters.config;

import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.client.reactive.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * @author Sachith Dickwella
 */
public class FilterClientResponse implements ClientHttpResponse {

    /**
     * {@link Flux} of {@link DataBuffer} instances.
     */
    private final Flux<DataBuffer> flux;
    /**
     * Http header list.
     */
    private final HttpHeaders headers;

    @SuppressWarnings("unchecked")
    private FilterClientResponse(Publisher<? extends DataBuffer> body, HttpHeaders headers) {
        this.headers = headers;
        if (body instanceof Flux) {
            flux = (Flux<DataBuffer>) body;
        } else {
            flux = ((Mono) body).flux();
        }
    }

    /**
     * Create a {@link FilterClientResponse} instance.
     *
     * @param body    {@link Publisher} instance
     * @param headers {@link HttpHeaders} instance
     * @return Instance of {@link FilterClientResponse}
     */
    @NotNull
    public static FilterClientResponse getInstance(Publisher<? extends DataBuffer> body, HttpHeaders headers) {
        return new FilterClientResponse(body, headers);
    }

    @NotNull
    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.OK;
    }

    @Override
    public int getRawStatusCode() {
        return HttpStatus.OK.value();
    }

    @SuppressWarnings("unchecked")
    @NotNull
    @Override
    public MultiValueMap<String, ResponseCookie> getCookies() {
        return (MultiValueMap) Map.of();
    }

    @NotNull
    @Override
    public Flux<DataBuffer> getBody() {
        return flux;
    }

    @NotNull
    @Override
    public HttpHeaders getHeaders() {
        return headers;
    }
}
