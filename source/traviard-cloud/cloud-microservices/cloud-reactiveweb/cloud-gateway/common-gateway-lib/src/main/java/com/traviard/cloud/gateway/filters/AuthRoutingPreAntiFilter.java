package com.traviard.cloud.gateway.filters;

import com.traviard.cloud.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static com.traviard.cloud.security.SecurityConstants.Cookies;

/**
 * This filter is use to block endpoints if the request included the
 * {@code TOKEN} cookie created after a successful login. Usually {@code login}
 * page blocks by this filter and if this filter is used by any of the services,
 * that particular service will be blocked by this filter after a successful
 * login and as a alternative, would redirect to a appropriate path respective
 * to the {@code Referer} header as this certain is using to enforce this filter's
 * core behaviour.
 *
 * @author Sachith Dickwella
 */
@Order
@Component
public class AuthRoutingPreAntiFilter extends AbstractNameValueGatewayFilterFactory {

    /**
     * WebFlux logging instance.
     */
    private final Logger logger;
    /**
     * RegEx pattern to extract URLs path from the {@code Referer} header
     * if available. This initialize {@code final} {@code static} {@link Pattern}
     * instance for filter to use.
     */
    private static final Pattern PATTERN_PATH = Pattern.compile("^(?:[^/]*(?:/(?:/[^/]*/?)?)?([^?]+)(?:\\??.+)?)$");

    /**
     * Argument constructor {@link @Autowire} the client and service name
     * instance to use for {@link Logger} instance while initializing it.
     *
     * @param logger {@link Logger} type instance.
     */
    @Autowired
    public AuthRoutingPreAntiFilter(Logger logger) {
        this.logger = logger;
    }

    /**
     * Factory method from the super-class do filter task. Implementation for
     * the filter goes here.
     *
     * @param config Instance of {@link NameValueConfig}
     * @return the instance of {@link GatewayFilter} class
     */
    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
            var request = exchange.getRequest();
            if (Optional.of(request.getHeaders().getAccept())
                    .stream()
                    .flatMap(List::stream)
                    .anyMatch(m -> m.includes(MediaType.TEXT_HTML))) {
                if (request.getCookies().containsKey(Cookies.SECURITY_TOKEN.getName())) {
                    return Optional.ofNullable(request.getHeaders().get(HttpHeaders.REFERER))
                            .stream()
                            .flatMap(List::stream)
                            .filter(StringUtils::isNotEmpty)
                            .map(m -> {
                                var matcher = PATTERN_PATH.matcher(m);
                                if (matcher.find()) {
                                    return authorizedRedirection(exchange, chain, String.format("/%s", matcher.group(1)));
                                }
                                return authorizedRedirection(exchange, chain);
                            })
                            .findFirst()
                            .orElseGet(() -> authorizedRedirection(exchange, chain));
                }
            }
            return chain.filter(exchange);
        };
    }

    /**
     * The default function to redirect to context path if the request doesn't
     * contain the {@code Referer} header. This functions simply invoke the overloaded
     * version of the same parameter list except the {@code redirectUrl} which
     * hardcoded the default context path for all the service endpoints.
     *
     * @param exchange Instance of {@link ServerWebExchange}
     * @param chain    Instance of {@link GatewayFilterChain}
     * @return an instance of {@link Mono<Void>} from downstream function
     */
    @NotNull
    private Mono<Void> authorizedRedirection(@NotNull ServerWebExchange exchange,
                                             @NotNull GatewayFilterChain chain) {
        return authorizedRedirection(exchange, chain, "/");
    }

    /**
     * The overloaded version of the {@code authorizedRedirection} function additionally
     * take {@code redirectUrl} for the custom url re-directions.
     *
     * @param exchange    Instance of {@link ServerWebExchange}
     * @param chain       Instance of {@link GatewayFilterChain}
     * @param redirectUrl {@link String} of redirect url
     * @return an instance of {@link Mono<Void>} from downstream function
     */
    @NotNull
    private Mono<Void> authorizedRedirection(@NotNull ServerWebExchange exchange,
                                             @NotNull GatewayFilterChain chain,
                                             @NotNull String redirectUrl) {
        var requestURI = exchange.getRequest().getURI();
        var response = exchange.getResponse();
        try {
            response.getHeaders()
                    .setLocation(new URI(
                            requestURI.getScheme(),
                            requestURI.getUserInfo(),
                            requestURI.getHost(),
                            requestURI.getPort(),
                            redirectUrl,
                            null, // Note: Do not forward query parameters with the redirect.
                            requestURI.getFragment()
                    ));
        } catch (URISyntaxException ex) {
            /*
             * This would never occur at runtime if the above details provided for the URI
             * builder are correct.
             */
            logger.error(() -> "URI syntax error", () -> ex);
        }
        return chain.filter(exchange.mutate().response(response).build())
                .then(Mono.fromRunnable(() -> response.setStatusCode(HttpStatus.FOUND)));
    }
}
