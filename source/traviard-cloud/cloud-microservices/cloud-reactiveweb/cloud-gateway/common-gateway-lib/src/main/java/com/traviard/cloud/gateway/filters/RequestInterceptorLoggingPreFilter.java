package com.traviard.cloud.gateway.filters;

import com.traviard.cloud.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractNameValueGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * @author Sachith Dickwella
 */
@Component
public class RequestInterceptorLoggingPreFilter extends AbstractNameValueGatewayFilterFactory implements Ordered {

    /**
     * WebFlux logging instance.
     */
    private final Logger logger;

    @Autowired
    public RequestInterceptorLoggingPreFilter(Logger logger) {
        this.logger = logger;
    }

    @Override
    public GatewayFilter apply(NameValueConfig config) {
        return (exchange, chain) -> {
            logger.info(() -> String.valueOf(exchange.getRequest()));
            return chain.filter(exchange);
        };
    }

    /**
     * Get the order value of this object.
     * <p>Higher values are interpreted as lower priority. As a consequence,
     * the object with the lowest value has the highest priority (somewhat
     * analogous to Servlet {@code load-on-startup} values).
     * <p>Same order values will result in arbitrary sort positions for the
     * affected objects.
     *
     * @return the order value
     * @see #HIGHEST_PRECEDENCE
     * @see #LOWEST_PRECEDENCE
     */
    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }
}
