package com.traviard.cloud.web.entity.repo;

import com.traviard.cloud.web.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface CurrencyRepo extends JpaRepository<Currency, Long> {
}
