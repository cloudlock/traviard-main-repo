package com.traviard.cloud.web.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("unused")
@Data
@Entity
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 35, nullable = false)
    private String name;

    @Column(length = 3, nullable = false)
    private String code;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "currency", cascade = {CascadeType.REMOVE})
    private List<Country> countries;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "preferredCurrency", cascade = {CascadeType.REMOVE})
    private List<User> users;

    @CreationTimestamp
    @Column(name = "created_datetime", nullable = false)
    private LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "lastupdated_datetime", nullable = false)
    private LocalDateTime lastUpdatedDateTime;
}