package com.traviard.cloud.web.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.jetbrains.annotations.Contract;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("unused")
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", length = 75, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 75, nullable = false)
    private String lastName;

    @Column(name = "user_image", columnDefinition = "MEDIUMBLOB")
    private byte[] userImage;

    @Column(length = 75, nullable = false)
    private String email;

    @Column(length = 200, nullable = false)
    private String password;

    @Column(name = "address_line1", length = 100, nullable = false)
    private String addressLine1;

    @Column(name = "address_line2", length = 100)
    private String addressLine2;

    @Column(name = "city_town", length = 75, nullable = false)
    private String cityTown;

    @Column(name = "zip_code", length = 20, nullable = false)
    private String zipCode;

    @Column(name = "telephone_number", length = 20, nullable = false)
    private String telephoneNumber;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = {CascadeType.REMOVE})
    private List<Card> cards;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "country_id", referencedColumnName = "id", nullable = false)
    private Country country;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "preferred_currency_id", referencedColumnName = "id")
    private Currency preferredCurrency;

    @Column(name = "is_active", nullable = false, columnDefinition = "TINYINT DEFAULT 1")
    private boolean isActive;

    @Column(name = "is_email_verified", nullable = false, columnDefinition = "TINYINT DEFAULT 0")
    private boolean isEmailVerified;

    @Column(name = "is_mobile_verified", nullable = false, columnDefinition = "TINYINT DEFAULT 0")
    private boolean isMobileVerified;

    @CreationTimestamp
    @Column(name = "created_datetime", nullable = false)
    private LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "lastupdated_datetime", nullable = false)
    private LocalDateTime lastUpdatedDateTime;

    public enum Gender {
        MALE("Male"),
        FEMALE("Female"),
        DO_NOT_PROVIDE("Don't Provide");

        private String description;

        Gender(String description) {
            this.description = description;
        }

        @Contract(pure = true)
        public String getDescription() {
            return this.description;
        }
    }
}
