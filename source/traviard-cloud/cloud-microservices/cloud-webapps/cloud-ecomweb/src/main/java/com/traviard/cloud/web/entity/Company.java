package com.traviard.cloud.web.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("unused")
@Data
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 75, nullable = false)
    private String name;

    @Column(columnDefinition = "MEDIUMBLOB")
    private byte[] logo;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(length = 100, nullable = false)
    private String email;

    @Column(length = 200, nullable = false)
    private String password;

    @Column(name = "work_number", length = 20, nullable = false)
    private String workNumber;

    @Column(name = "mobile_number", length = 20, nullable = false)
    private String mobileNumber;

    @Column(name = "address_line1", length = 100, nullable = false)
    private String addressLine1;

    @Column(name = "address_line2", length = 100)
    private String addressLine2;

    @Column(name = "city_town", length = 75, nullable = false)
    private String cityTown;

    @Column(name = "zip_code", length = 20, nullable = false)
    private String zipCode;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "country_id", referencedColumnName = "id", nullable = false)
    private Country country;

    @Column(name = "is_active", nullable = false, columnDefinition = "TINYINT DEFAULT 1")
    private boolean isActive;

    @Column(name = "is_email_verified", nullable = false, columnDefinition = "TINYINT DEFAULT 0")
    private boolean isEmailVerified;

    @Column(name = "is_mobile_verified", nullable = false, columnDefinition = "TINYINT DEFAULT 0")
    private boolean isMobileVerified;

    @Column(name = "is_work_verified", nullable = false, columnDefinition = "TINYINT DEFAULT 0")
    private boolean isWorkVerified;

    @CreationTimestamp
    @Column(name = "created_datetime", nullable = false)
    private LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "lastupdated_datetime", nullable = false)
    private LocalDateTime lastUpdatedDateTime;
}
