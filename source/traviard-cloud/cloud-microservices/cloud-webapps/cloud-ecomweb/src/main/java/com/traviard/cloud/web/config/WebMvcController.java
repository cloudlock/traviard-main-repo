package com.traviard.cloud.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Sachith Dickwella
 */
@Controller
@PropertySource("classpath:config.properties")
public class WebMvcController {

    @Value("${my.application.appName}")
    private String appName;

    @Value("${my.application.home.title}")
    private String homeTitle;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("appName", appName)
                .addAttribute("title", homeTitle);
        return "index";
    }
}
