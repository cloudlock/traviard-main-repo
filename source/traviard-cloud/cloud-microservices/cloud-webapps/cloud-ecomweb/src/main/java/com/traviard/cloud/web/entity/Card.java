package com.traviard.cloud.web.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.jetbrains.annotations.Contract;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("unused")
@Data
@Entity
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 40, nullable = false)
    private String number;

    @Column(name = "card_type", nullable = false)
    private Type cardType;

    @Column(length = 3, nullable = false)
    private Integer cvs;

    @Column(name = "expire_date", nullable = false)
    private LocalDate expireDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false)
    private User user;

    @CreationTimestamp
    @Column(name = "created_datetime", nullable = false)
    private LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "lastupdated_datetime", nullable = false)
    private LocalDateTime lastUpdatedDateTime;

    public enum Type {

        MASTER("Master Card"),
        VISA("VISA Card"),
        AMEX("American Express"),
        FRIMI("FriMi");

        private String description;

        Type(String description) {
            this.description = description;
        }

        @Contract(pure = true)
        public String getDescription() {
            return this.description;
        }
    }
}
