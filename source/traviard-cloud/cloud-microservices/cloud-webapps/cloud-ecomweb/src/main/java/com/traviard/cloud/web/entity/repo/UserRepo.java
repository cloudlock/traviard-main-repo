package com.traviard.cloud.web.entity.repo;

import com.traviard.cloud.web.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
