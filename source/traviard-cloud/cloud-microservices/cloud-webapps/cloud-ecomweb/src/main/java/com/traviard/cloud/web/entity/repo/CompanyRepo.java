package com.traviard.cloud.web.entity.repo;

import com.traviard.cloud.web.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface CompanyRepo extends JpaRepository<Company, Long> {
}
