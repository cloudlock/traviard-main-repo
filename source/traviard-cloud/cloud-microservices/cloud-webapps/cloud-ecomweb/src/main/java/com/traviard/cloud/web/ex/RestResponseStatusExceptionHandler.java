package com.traviard.cloud.web.ex;

import com.traviard.cloud.exhandler.GlobalExceptionHandler;
import com.traviard.cloud.exhandler.util.HttpErrorResponse;
import com.traviard.cloud.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sachith Dickwella
 */
@ControllerAdvice
public class RestResponseStatusExceptionHandler extends GlobalExceptionHandler {

    /**
     * Autowiring instance to the constructor.
     *
     * @param logger      {@link Logger} instance
     */
    @Autowired
    public RestResponseStatusExceptionHandler(Logger logger) {
        super(logger);
    }

    /**
     * Global exception handler for service RuntimeExceptions.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({RuntimeException.class})
    @Override
    public ResponseEntity<HttpErrorResponse> handleRuntimeException(HttpServletRequest request, RuntimeException ex) {
        return super.handleRuntimeException(request, ex);
    }
}
