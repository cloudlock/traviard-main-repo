package com.traviard.cloud.web.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.boot.autoconfigure.security.SecurityProperties;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("unused")
@Data
@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 45, nullable = false)
    private String name;

    @Column(length = 7, nullable = false)
    private String code;

    @Column(name = "telephone_code", length = 7, nullable = false)
    private String telephoneCode;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "currency_id", referencedColumnName = "id", nullable = false)
    private Currency currency;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country", cascade = {CascadeType.REMOVE})
    private List<User> users;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country", cascade = {CascadeType.REMOVE})
    private List<Company> companies;

    @CreationTimestamp
    @Column(name = "created_datetime", nullable = false)
    private LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "lastupdated_datetime", nullable = false)
    private LocalDateTime lastUpdatedDateTime;
}