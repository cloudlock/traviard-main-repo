package com.traviard.cloud.web.config;

import org.springframework.context.annotation.Configuration;

/**
 * Client-side load balancing configuration for {@code cloud-logger} micro-service.
 *
 * @author Sachith Dickwella
 */
@Configuration
public class LoggerRibbonClientConfig {

    /*
     * TODO Ribbon configurations.
     */
}
