package com.traviard.cloud.web.datastore;

import com.traviard.cloud.log4j.Logger;
import com.traviard.cloud.log4j.logger.ServiceLogger;
import com.traviard.cloud.log4j.util.Type;
import com.traviard.cloud.web.datastore.config.LoggerRibbonClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author Sachith Dickwella
 */
@EnableEurekaClient
@ComponentScan({
        "com.traviard.cloud.security",
        "com.traviard.cloud.web.datastore.config",
        "com.traviard.cloud.web.datastore.controller",
        "com.traviard.cloud.web.datastore.ex"
})
@RibbonClient(name = "LoggerRibbonClient", configuration = LoggerRibbonClientConfig.class)
@EnableAutoConfiguration
public class DataStoreWebApp {

    @Value("${spring.application.name}")
    private String serviceName;

    public static void main(String[] args) {
        SpringApplication.run(DataStoreWebApp.class, args);
    }

    @LoadBalanced
    @Bean
    public WebClient getWebClient(LoadBalancerClient loadBalancerClient) {
        return WebClient.builder()
                .filter(new LoadBalancerExchangeFilterFunction(loadBalancerClient))
                .build();
    }

    @Bean("serviceName")
    public String getServiceName() {
        return serviceName;
    }

    @Bean
    public Logger getServiceLogger(WebClient cient, String serviceName) {
        return new ServiceLogger(cient, serviceName, Type.SERVICE);
    }
}
