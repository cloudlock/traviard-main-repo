package com.traviard.cloud.web.datastore.ex;

import com.traviard.cloud.exhandler.GlobalExceptionHandler;
import com.traviard.cloud.exhandler.util.HttpErrorResponse;
import com.traviard.cloud.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sachith Dickwella
 */
@ControllerAdvice
public class RestResponseStatusExceptionHandler extends GlobalExceptionHandler {

    /**
     * Autowiring instances to the constructor.
     *
     * @param logger      {@link Logger} instance
     */
    @Autowired
    public RestResponseStatusExceptionHandler(Logger logger) {
        super(logger);
    }

    /**
     * Global exception handler for service RuntimeExceptions.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({RuntimeException.class})
    @Override
    public ResponseEntity<HttpErrorResponse> handleRuntimeException(HttpServletRequest request, RuntimeException ex) {
        return super.handleRuntimeException(request, ex);
    }

    /**
     * NullPointer exception global handler.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({NullPointerException.class})
    @Override
    public ResponseEntity<HttpErrorResponse> handleNullPointerException(HttpServletRequest request, NullPointerException ex) {
        return super.handleNullPointerException(request, ex);
    }

    /**
     * Global handler for {@link HttpClientErrorException.Unauthorized} for client invocations.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link HttpClientErrorException.Unauthorized} instance that could throws
     *                from the service
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({HttpClientErrorException.Unauthorized.class})
    public ResponseEntity<HttpErrorResponse> handleUnauthorizedClientErrorException(
            HttpServletRequest request,
            @NotNull HttpClientErrorException.Unauthorized ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        logger.error(status::getReasonPhrase, ex::getCause);
        return getResponseEntity(request, status);
    }
}
