package com.traviard.cloud.web.datastore.controller;

import com.traviard.cloud.security.Config;
import com.traviard.cloud.security.SecurityEndpointConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.traviard.cloud.security.SecurityConstants.Cookies.CSRF_TOKEN;
import static com.traviard.cloud.security.SecurityConstants.Cookies.SECURITY_TOKEN;

/**
 * @author Sachith Dickwella
 */
@RestController
@RequestMapping("/user")
public class UserClientRestController {

    /**
     * Load-balanced client to access logout endpoint.
     */
    private WebClient client;
    /**
     * Common configuration {@link Config} object from {@code securityclient-lib}
     * module.
     */
    private Config config;
    /**
     * Security service endpoint configuration instance which represents by
     * {@link SecurityEndpointConfig} instance.
     */
    private SecurityEndpointConfig securityEndpointConfig;

    /**
     * Autowired constructor to initialize the load-balanced client and the common
     * configuration instance.
     *
     * @param client                 instance of {@link WebClient}
     * @param config                 instance of {@link Config}
     * @param securityEndpointConfig instance of {@link SecurityEndpointConfig}
     */
    @Autowired
    public UserClientRestController(WebClient client,
                                    Config config,
                                    SecurityEndpointConfig securityEndpointConfig) {
        this.client = client;
        this.config = config;
        this.securityEndpointConfig = securityEndpointConfig;
    }

    /**
     * Local endpoint to use by own application to perform logout process.
     *
     * @param request Instance of {@link HttpServletRequest} from original request
     * @return an instance of {@link Mono} with the downstream response
     */
    @PostMapping("/logout")
    public Mono<ResponseEntity<Void>> logout(@NotNull HttpServletRequest request) {
        final Set<HttpCookie> cookies = Stream.of(request.getCookies())
                .filter(Objects::nonNull)
                .filter(cookie -> cookie.getName().equals(SECURITY_TOKEN.getName())
                        || cookie.getName().equals(CSRF_TOKEN.getName()))
                .map(cookie -> new HttpCookie(cookie.getName(), cookie.getValue()))
                .collect(Collectors.toSet());

        return client.post()
                .uri(String.format("%s/logout", securityEndpointConfig.getUserBaseURL()))
                .headers(e -> e.addAll(config.validHeaders(cookies)))
                .retrieve()
                .toEntity(Void.class);
    }
}
