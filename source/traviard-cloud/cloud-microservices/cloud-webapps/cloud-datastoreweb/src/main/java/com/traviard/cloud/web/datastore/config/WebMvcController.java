package com.traviard.cloud.web.datastore.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * @author Sachith Dickwella
 */
@Controller
@PropertySource("classpath:config.properties")
public class WebMvcController {

    /**
     * Application name.
     */
    @Value("${my.application.name}")
    private String name;
    /**
     * Home page title name.
     */
    @Value("${my.application.home.title}")
    private String homeTitle;
    /**
     * Storage page title name.
     */
    @Value("${my.application.storage.title}")
    private String storageTitle;

    /**
     * Modeler for the home page.
     *
     * @param model Instance of {@link Model}
     * @return the {@link String} of the page name
     */
    @GetMapping(path = {"/", "/index", "/index.html"})
    public String index(@NotNull Model model) {
        model.addAttribute("name", name);
        model.addAttribute("title", homeTitle);
        model.addAttribute("thisYear", LocalDate.now().getYear());
        return "index";
    }

    /**
     * Modeler for the storage page.
     *
     * @param model   Instance of {@link Model}
     * @param request Instance of {@link HttpServletRequest} from the request
     * @return the {@link String} of the page name
     */
    @GetMapping(path = {"/storage", "/storage/index", "/storage/index.html"})
    public String storage(@NotNull Model model, @NotNull HttpServletRequest request) {
        model.addAttribute("name", name);
        model.addAttribute("title", storageTitle);
        model.addAttribute("thisYear", LocalDate.now().getYear());
        return "storage/index";
    }
}