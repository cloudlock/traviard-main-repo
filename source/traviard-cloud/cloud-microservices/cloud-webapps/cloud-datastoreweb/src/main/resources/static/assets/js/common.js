const goToStore = () => window.location = `${window.location.pathname}/storage`;
const signOut = (e) => {
    $.ajax({
        url: `${window.location.pathname}/../user/logout`,
        method: 'POST',
        beforeSend: (jqXHR) => {
            // $(e).html('<i class="fa fa-refresh fa-spin fa-fw font-size17px"></i>') # TODO Valid implementation for later.
        },
        complete: () => {
            window.location = "/";
        }
    });
};