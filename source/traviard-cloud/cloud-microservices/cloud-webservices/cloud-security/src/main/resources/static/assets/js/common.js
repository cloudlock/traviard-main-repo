const signIn = () => {
    const btnLogin = $('#btn-login'),
        errorCard = $('.failed-login'),
        errorCardMessage = $('.failed-login-msg');
    $.ajax({
        url: `${window.location.pathname}/user/login`,
        method: 'POST',
        data: {'email': $('#email').val(), 'password': $('#password').val()},
        beforeSend:
            (jqXHR) => {
                jqXHR.setRequestHeader(
                    $("meta[name='_csrf_headerName']").attr('content'),
                    $("meta[name='_csrf']").attr('content'));
                if (errorCard.css('display') === 'block') {
                    $(errorCard.css('display', 'none'));
                }
                btnLogin.html('<i class="fa fa-refresh fa-spin fa-fw font-size17px"></i>')
                    .addClass('disabled');
            },
        success:
            () => {
                const ref = getQueryParamByName('ref');

                if (!ref) window.location = '/';
                else window.location = ref;
            },
        error:
            (jqXHR) => {
                errorCard.css('display', 'block');
                if (jqXHR.status === 401) {
                    errorCardMessage.text('Username or password is invalid');
                } else if (jqXHR.status === 503) {
                    errorCardMessage.text('Auth service not available, contact support');
                } else {
                    errorCardMessage.text('Something went wrong, contact support');
                }
            },
        complete:
            () => {
                btnLogin.html('<i class="fas fa-unlock-alt white-text"></i>&nbsp;&nbsp;&nbsp;Sign in')
                    .removeClass('disabled');
            }
    });
};
let payload;
const personalRegister = () => {
    const ol = $('.overlay2');
    $.ajax({
        url: `${window.location.pathname}/../user/create`,
        headers: {
            'Accept': 'application/json; charset=utf-8',
            'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        data: JSON.stringify(payload),
        beforeSend:
            (jqXHR) => {
                ol.removeClass('d-none');
                jqXHR.setRequestHeader(
                    $("meta[name='_csrf_headerName']").attr('content'),
                    $("meta[name='_csrf']").attr('content'));
            },
        success:
            (data, textStatus, jqXHR) => {
                if (jqXHR.status === 200) {
                    console.log(jqXHR);
                } else if (jqXHR.status === 201) {

                }
            },
        error:
            (jqXHR) => {

            },
        complete:
            (jqXHR, textStatus) => {
                ol.addClass('d-none');
            }
    });
};

const redirect = (e) => window.location = `${window.location.pathname}/${e}${window.location.search}`;
const getQueryParamByName = (name, url) => {
    if (!url) url = decodeURIComponent(window.location.href);

    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
    const result = regex.exec(url);

    if (!result || !result[2]) return undefined;
    return decodeURIComponent(result[2].replace(/\+/g, ' '));
};

const pwd = (e) => {
    const fa = $(e);
    const parent = fa.parent();
    const input = parent.children('input')[0];

    $(input).remove();

    if (fa.hasClass('fa-eye-slash')) {
        fa.removeClass('fa-eye-slash').addClass('fa-eye');
        $(input).attr('type', 'text');

        if ($(input).attr('id') === 'passwd') passwd.focusout(() => isValidPasswd(true));
        else if ($(input).attr('id') === 'conf-passwd') conf_passwd.focusout(() => isValidConfPasswd(true));
    } else {
        fa.removeClass('fa-eye').addClass('fa-eye-slash');
        $(input).attr('type', 'password');

        if ($(input).attr('id') === 'passwd') passwd.focusout(() => isValidPasswd(true));
        else if ($(input).attr('id') === 'conf-passwd') conf_passwd.focusout(() => isValidConfPasswd(true));
    }
    parent.prepend($(input).get(0));
};

const addInvalidClass = (e) => e.parent('div').addClass('invalid-input');
const removeInvalidClass = (e) => e.parent('div').removeClass('invalid-input');

const isValidPersonalRegister = () => {
    let isValid = true;
    isValid &= isValidFName(isValid)
        & isValidLName(isValid)
        & isValidUName(isValid)
        & isValidAltEmail(isValid)
        & isValidPasswd(isValid)
        & isValidConfPasswd(isValid)
        & isValidMobileNo(isValid)
        & isValidBDay(isValid);

    if (isValid) {
        payload = {
            firstName: fname.val().trim(),
            lastName: lname.val().trim(),
            userName: uname.val().trim(),
            altEmail: altEmail.val().trim(),
            password: passwd.val().trim(),
            mobileNumber: country_value.val().trim() + mobile_number.val().trim(),
            dateOfBirth: `${in_day.val().trim()}/${months.indexOf(in_month.val().trim()) + 1}/${in_year.val().trim()}`, /* dd/MM/yyyy */
            gender: gender.val().trim(),
            country: {id: country_value.attr('data-id')}
        };
        $('.register').addClass('d-none');
        $('.license').removeClass('d-none');
    }
    return isValid;
};

const isValidName = (e, v, str) => {
    const nameVal = v.val().trim();
    const err = errEle(v);
    if (!nameVal || nameVal === '') {
        e = false;
        err.text(`${str} is required`);
    } else if (nameVal.length > 20) {
        e = false;
        err.text(`${str} is too long`);
    } else if (!/^([a-zA-Z\s])+$/.test(nameVal)) {
        e = false;
        err.text(`Only letters allowed`);
    } else {
        err.text('');
    }

    if (!e) addInvalidClass(v);
    else removeInvalidClass(v);

    return e;
};

const isValidFName = (e) => isValidName(e, fname, 'First name');
const isValidLName = (e) => isValidName(e, lname, 'Last name');

const isValidUName = (e) => {
    const unameVal = uname.val().trim();
    const err = errEle(uname);
    if (!unameVal || unameVal === '' || !/^([a-zA-Z0-9_.+-])+$/.test(unameVal)) {
        e = false;
        err.addClass('error');
        err.text('Letters, numbers, periods, hypens and underscores only');
    } else if (unameVal.length > 50) {
        e = false;
        err.addClass('error');
        err.text('Username is too long');
    } else if (unameVal.length < 6) {
        e = false;
        err.addClass('error');
        err.text('Username should at least 6 letters long');
    } else {
        const ol = $('.overlay1');
        $.ajax({
            url: `${window.location.pathname}/../user/exists/${unameVal}`,
            method: 'GET',
            async: false,
            beforeSend: () => {
                ol.removeClass('d-none');
            },
            success:
                (data, textStatus, jqXHR) => {
                    if (jqXHR.status === 204) {
                        err.addClass('error');
                        err.text('Username is not available');
                        e = false;
                    }
                },
            complete: () => {
                ol.addClass('d-none')
            }
        });
    }

    if (!e) addInvalidClass(uname);
    else {
        err.removeClass('error');
        err.text('Letters, numbers, periods, hypens and underscores only');
        removeInvalidClass(uname);
    }

    return e;
};

const isValidAltEmail = (e) => {
    const emailVal = altEmail.val().trim();
    const err = errEle(altEmail);
    if (!emailVal || emailVal === '') {
        e = false;
        err.addClass('error');
        err.text('Alternative email is required to recover login info');
    } else if (emailVal.length > 60) {
        e = false;
        err.addClass('error');
        err.text('Alternative email is too long');
    } else if (!/^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(emailVal)) {
        e = false;
        err.addClass('error');
        err.text('Valid email \'john@example.com\'');
    } else {
        err.removeClass('error');
        err.text('Valid email \'john@example.com\'');
    }

    if (!e) addInvalidClass(altEmail);
    else removeInvalidClass(altEmail);

    return e;
};

const isValidPasswd = (e) => {
    const passwdVal = passwd.val().trim();
    const err = errEle(passwd);
    if (!passwdVal || passwdVal === '' || passwdVal.length < 6) {
        e = false;
        err.addClass('error');
    } else {
        err.removeClass('error');
    }

    if (!e) addInvalidClass(passwd);
    else removeInvalidClass(passwd);
    return e;
};

const arePasswdsMatch = (e) => {
    const confPasswdVal = conf_passwd.val().trim();
    const passwdVal = passwd.val().trim();
    const err = errEle(conf_passwd);
    if (passwdVal !== '' && passwdVal !== confPasswdVal) {
        e = false;
        err.text('Password does not match');
    } else {
        err.text('');
    }

    if (!e) addInvalidClass(conf_passwd);
    else removeInvalidClass(conf_passwd);

    return e;
};

const isValidConfPasswd = (e) => {
    const confPasswdVal = conf_passwd.val().trim();
    if (!confPasswdVal || confPasswdVal === '') {
        e = false;
        addInvalidClass(conf_passwd);
    } else {
        removeInvalidClass(conf_passwd);
    }
    return arePasswdsMatch(e);
};

const isValidMobileNo = (e) => {
    const mobileNumberVal = mobile_number.val().trim();
    const err = errEle(mobile_number);
    if (mobileNumberVal && mobileNumberVal.length >= 8 && !/^([1-9])([0-9]{8,15})$/.test(mobileNumberVal)) {
        e = false;
        err.addClass('error').removeClass('optional');
        err.text('Invalid phone number');
    } else if (mobileNumberVal && mobileNumberVal.length < 8) {
        e = false;
        err.addClass('error').removeClass('optional');
        err.text('Phone number is too short');
    } else {
        err.removeClass('error').addClass('optional');
        err.text('Optional - Neither country code nor leading 0 required');
    }

    if (!e) addInvalidClass(mobile_number);
    else removeInvalidClass(mobile_number);

    return e;
};

const isValidDay = (e) => {
    const dayVal = in_day.val().trim();
    if (!dayVal || dayVal === '' || dayVal.length > 2
        || (parseInt(dayVal) > 31) || parseInt(dayVal) < 1) {
        e = false;
    } else {
        if (dayVal.length === 1) in_day.val(`0${dayVal}`);
    }

    if (!e) addInvalidClass(in_day);
    else removeInvalidClass(in_day);

    return e;
};

const isValidMonth = (e) => {
    const monthVal = in_month.val().trim();
    if (!months.includes(monthVal)) {
        e = false;
    }

    if (!e) addInvalidClass(in_month);
    else removeInvalidClass(in_month);
    return e;
};

const isValidYear = (e) => {
    const yearVal = in_year.val().trim();
    if (!yearVal || yearVal === '' || yearVal.length > 4
        || parseInt(yearVal) < 1950 || parseInt(yearVal) > (new Date().getUTCFullYear() - 10)) {
        e = false;
    }

    if (!e) addInvalidClass(in_year);
    else removeInvalidClass(in_year);
    return e;
};

const isValidBDay = (e, i) => {
    let isValid = true;
    if (i === 'day') isValid &= isValidDay(e);
    else if (i === 'month') isValid &= isValidDay(e) & isValidMonth(e);
    else if (i === 'year') isValid &= isValidDay(e) & isValidMonth(e) & isValidYear(e);
    else {
        isValid &= isValidDay(e)
            & isValidMonth(e)
            & isValidYear(e);
    }

    const err = $('.bday-msg');
    if (!isValid) err.removeClass('invisible');
    else err.addClass('invisible');
    return isValid;
};

const months = ['January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August', 'September', 'October',
    'November', 'December'];

const fname = $('#fname');
const lname = $('#lname');
const uname = $('#email');
const altEmail = $('#alt-email');
const passwd = $('#passwd');
const conf_passwd = $('#conf-passwd');
const mobile_number = $('#mobile-number');
const in_day = $('#day');
const in_month = $('#month');
const in_year = $('#year');
const gender = $('#gender');
const country_value = $('#country-code');

/**
 fname.focusout(() => isValidFName(true));
 lname.focusout(() => isValidLName(true));
 uname.focusout(() => isValidUName(true));
 altEmail.focusout(() => isValidAltEmail(true));
 passwd.focusout(() => isValidPasswd(true));
 conf_passwd.focusout(() => isValidConfPasswd(true));
 mobile_number.focusout(() => isValidMobileNo(true));
 in_day.on('focusout focusin', () => isValidBDay(true, 'day'));
 in_month.on('focusout focusin', () => isValidBDay(true, 'month'));
 in_year.on('focusout focusin', () => isValidBDay(true, 'year'));
 */

const errEle = (e) => $(e.parent('div').parent('div').children('.additional-message')[0]);
const isNumber = (e) => (!e.shiftKey && e.keyCode >= 48 && e.keyCode <= 57)
    || (!e.shiftKey && e.keyCode >= 96 && e.keyCode <= 105)
    || e.keyCode === 8 || e.keyCode === 9 || e.keyCode === 46
    || e.keyCode === 37 || e.keyCode === 39;
in_day.keydown((e) => {
    if (!isNumber(e)) e.preventDefault()
});
in_year.keydown((e) => {
    if (isNumber(e)) {
        const ct = $(e.currentTarget),
            val = ct.val().trim();
        if (val === '' && e.keyCode < 49 && e.keyCode > 48) e.preventDefault();
    } else {
        e.preventDefault();
    }
});
mobile_number.keydown((e) => {
    if (!isNumber(e)) e.preventDefault();
});
