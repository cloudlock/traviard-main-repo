package com.traviard.cloud.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.traviard.cloud.facade.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Sachith Dickwella
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Document("country")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Country extends Entity<Country> {

    public Country() {
        super(Country.class);
    }

    @Id
    private String id;

    @Field("n")
    @Indexed
    private String name;

    @Field("cc")
    @Indexed
    private String countryCode;

    @Field("ic")
    @Indexed
    private String isoCode;

    @DBRef(lazy = true)
    @JsonIgnore
    private List<User> users;

    @Field("cdt")
    @CreatedDate
    private LocalDateTime createDateTime;

    @Field("ldt")
    @LastModifiedDate
    private LocalDateTime lastUpdateDateTime;

    /**
     * Declare the getter for Kotlin consumers.
     */
    public String getId() {
        return id;
    }
}
