package com.traviard.cloud.security.config;

import com.traviard.cloud.security.entity.repo.CountryRepo;
import com.traviard.cloud.security.util.Login;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

import static com.traviard.cloud.util.WebAppNames.getAppName;

/**
 * @author Sachith Dickwella
 */
@Controller
@PropertySource("classpath:config.properties")
public class WebMvcController {

    @Value("${my.application.appName}")
    private String appName;

    @Value("${my.application.login.title}")
    private String authUiTitle;

    @Value("${my.application.register.title}")
    private String registerUiTitle;

    private CountryRepo repo;

    @Autowired
    public WebMvcController(CountryRepo repo) {
        this.repo = repo;
    }

    /**
     * Modeling an instance for the {@code index.html} page of the application.
     * Also know as the {@code login} page of the platform.
     *
     * @param model inject a {@link Model} instance by the spring context
     * @return a {@link String} object of the page reference
     */
    @GetMapping(path = {"/", "/index", "/index.html"})
    public String login(@NotNull Model model, @NotNull HttpServletRequest request) {
        model.addAttribute("app", appName)
                .addAttribute("title", authUiTitle)
                .addAttribute("login", new Login())
                .addAttribute("appName", getAppName(request));

        return "index";
    }

    /**
     * Modeling an instance for the {@code index.html} page of the application.
     * Also know as the {@code register} page of the platform, originally located
     * at {@code register/index.html}.
     *
     * @param model inject a {@link Model} instance by the spring context
     * @return a {@link String} object of the page reference
     */
    @GetMapping(path = {"/register", "/register/index", "/register/index.html"})
    public String register(@NotNull Model model, @NotNull HttpServletRequest request) {
        model.addAttribute("app", appName)
                .addAttribute("title", registerUiTitle)
                .addAttribute("countries", repo.findAllAndExcludeIdAndDates())
                .addAttribute("client_country", repo.findByNameAndExcludeAll("United States").getId())
                .addAttribute("appName", getAppName(request));

        return "register/index";
    }
}