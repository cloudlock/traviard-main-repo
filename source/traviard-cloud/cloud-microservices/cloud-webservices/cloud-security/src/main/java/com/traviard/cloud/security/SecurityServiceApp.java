package com.traviard.cloud.security;

import com.traviard.cloud.log4j.Logger;
import com.traviard.cloud.log4j.logger.ServiceLogger;
import com.traviard.cloud.log4j.util.Type;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author Sachtih Dickwella
 */
@EnableMongoAuditing
@EnableEurekaClient
@ComponentScan({
        "com.traviard.cloud.security",
        "com.traviard.cloud.security.config",
        "com.traviard.cloud.security.entity",
        "com.traviard.cloud.security.entity.validation.impl",
        "com.traviard.cloud.security.controller",
        "com.traviard.cloud.security.ex",
        "com.traviard.cloud.util"
})
@EnableAutoConfiguration
public class SecurityServiceApp {

    @Value("${spring.application.name}")
    private String serviceName;

    public static void main(String[] args) {
        SpringApplication.run(SecurityServiceApp.class, args);
    }

    @Bean("serviceName")
    public String getServiceName() {
        return serviceName;
    }

    @Bean
    public Logger getServiceLogger(WebClient client, String serviceName) {
        return new ServiceLogger(client, serviceName, Type.SERVICE);
    }

    @LoadBalanced
    @Bean
    public WebClient getWebClient(LoadBalancerClient loadBalancerClient) {
        return WebClient.builder()
                .filter(new LoadBalancerExchangeFilterFunction(loadBalancerClient))
                .build();
    }
}
