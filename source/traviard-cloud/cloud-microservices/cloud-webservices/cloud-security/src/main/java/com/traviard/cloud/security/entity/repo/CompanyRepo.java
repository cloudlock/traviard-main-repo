package com.traviard.cloud.security.entity.repo;

import com.traviard.cloud.repository.CommonRepository;
import com.traviard.cloud.security.entity.Company;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface CompanyRepo extends MongoRepository<Company, String>, CommonRepository<String> {

    /**
     * Check if the entity represent by the parameter {@code id} is available in the database.
     *
     * @param id entity id
     * @return the availability status of the entity.
     */
    @ExistsQuery("{ id: ?0 }")
    @Override
    Boolean isAvailable(String id);
}
