package com.traviard.cloud.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.traviard.cloud.facade.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Sachith Dickwella
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Document("company")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company extends Entity<Company> {

    public Company() {
        super(Company.class);
    }

    @Id
    private String id;

    private String name;

    @Field("al1")
    private String addressLine1;

    @Field("al2")
    private String addressLine2;

    @DBRef(lazy = true)
    private List<Company> companies;

    @Field("cdt")
    @CreatedDate
    private LocalDateTime createDateTime;

    @Field("ldt")
    @LastModifiedDate
    private LocalDateTime lastUpdateDateTime;

    /**
     * Declare the getter for Kotlin consumers.
     */
    public String getId() {
        return id;
    }
}
