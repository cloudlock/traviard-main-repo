package com.traviard.cloud.security.util;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Sachith Dickwella
 */
@Data
public class Login implements Serializable {

    /**
     * {@code serialVersionUID} instance.
     */
    private static final long serialVersionUID = 330587073766193346L;
    /**
     * Login user's email address, unique to every user.
     */
    private String email;
    /**
     * User's login password.
     */
    private String password;
}
