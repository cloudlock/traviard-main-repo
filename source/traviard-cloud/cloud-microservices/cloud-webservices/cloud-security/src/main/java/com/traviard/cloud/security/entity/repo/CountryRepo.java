package com.traviard.cloud.security.entity.repo;

import com.traviard.cloud.repository.CommonRepository;
import com.traviard.cloud.security.entity.Country;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface CountryRepo extends MongoRepository<Country, String>, CommonRepository<String> {

    /**
     * Fetch all the countries with no condition. Exclude fields {@code _id, createdDateTime}
     * and {@code lastUpdateDateTime}.
     *
     * @return {@link List<Country>} instance with all the countries
     */
    @Query(value = "{}", fields = "{ id: 1, name: 1, countryCode: 1, isoCode: 1 }")
    List<Country> findAllAndExcludeIdAndDates();

    /**
     * Fetch specific {@link Country} instance from the database by the
     * {@code name} attribute and exclude all else.
     *
     * @param name {@link String} country name
     * @return an instance of {@link Country}
     */
    @Query(value = "{ name: ?0 }", fields = "{ id: 1 }")
    Country findByNameAndExcludeAll(String name);

    /**
     * Check if the entity represent by the parameter {@code id} is available in the database.
     *
     * @param id entity id
     * @return the availability status of the entity.
     */
    @ExistsQuery("{ id: ?0 }")
    @Override
    Boolean isAvailable(String id);
}
