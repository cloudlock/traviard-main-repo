package com.traviard.cloud.security.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.traviard.cloud.facade.Entity;
import com.traviard.cloud.util.EntityUtils;
import com.traviard.cloud.validation.SkipValidation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.types.Binary;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Sachith Dickwella
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Document("user")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends Entity<User> {

    public User() {
        super(User.class);
    }

    @Id
    private String id;

    @Indexed
    private String firstName;

    @Indexed
    private String lastName;

    @Field("un")
    @Indexed
    private String userName;

    @Indexed
    private String altEmail;

    private String password;

    private String mobileNumber;

    @JsonFormat(pattern = "dd/M/yyyy")
    @Field("dob")
    private LocalDate dateOfBirth;

    private String gender;

    private Binary profilePicture;

    @Field("country")
    @DBRef
    private Country country;

    @Field("comp")
    @DBRef
    private Company company;

    @SkipValidation
    @CreatedDate
    private LocalDateTime createDateTme;

    @SkipValidation
    @LastModifiedDate
    private LocalDateTime lastUpdateDateTime;

    /**
     * Return stringify Java object.
     *
     * @return Stringify json object to string
     */
    private String toJson() {
        return EntityUtils.toJson(this);
    }

    /**
     * Implementation of {@code toString}.
     *
     * @return Json representation of the object
     */
    @Override
    public String toString() {
        return toJson();
    }
}