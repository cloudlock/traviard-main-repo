package com.traviard.cloud.security.entity.repo;

import com.traviard.cloud.repository.CommonRepository;
import com.traviard.cloud.security.entity.User;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Sachith Dickwella
 */
@Repository
public interface UserRepo extends MongoRepository<User, String>, CommonRepository<String> {

    /**
     * Get an {@link Optional} instance of {@link User} by username address.
     *
     * @param userName {@link User}'s email address
     * @return an {@link Optional} instance of {@link User}
     */
    Optional<User> findOptionalByUserName(String userName);

    /**
     * Get {@link Boolean} instance of count of fetching instance.
     *
     * @param userName {@link User}'s email address
     * @return count of corresponding {@link User} instances for the username
     */
    @ExistsQuery("{ userName: ?0 }")
    Boolean isUserAvailable(@Param("userName") String userName);

    /**
     * Check if the entity represent by the parameter {@code id} is available in the database.
     *
     * @param id entity id
     * @return the availability status of the entity.
     */
    @ExistsQuery("{ id: ?0 }")
    @Override
    Boolean isAvailable(String id);
}
