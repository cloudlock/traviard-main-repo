package com.traviard.cloud.security.controller;

import com.traviard.cloud.facade.CrudRepositoryAbstractController;
import com.traviard.cloud.facade.Response;
import com.traviard.cloud.log4j.Logger;
import com.traviard.cloud.security.entity.User;
import com.traviard.cloud.security.entity.repo.UserRepo;
import com.traviard.cloud.security.entity.validation.impl.UserValidation;
import com.traviard.cloud.security.util.Login;
import com.traviard.cloud.security.util.Token;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.traviard.cloud.security.SecurityConstants.COOKIE_MAX_AGE;
import static com.traviard.cloud.security.SecurityConstants.Cookies.SECURITY_TOKEN;
import static com.traviard.cloud.security.SecurityConstants.Headers.X_AUTHORIZATION;
import static com.traviard.cloud.util.ApplicationConstants.appendDomain;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Sachith Dickwella
 */
@RefreshScope
@PropertySource("classpath:security-config.properties")
@RestController
@RequestMapping("/user")
public class UserController extends CrudRepositoryAbstractController<User, String> {

    /**
     * User id reference for JWT token claim.
     */
    private static final String ID = "id";
    /**
     * User name reference for JWT token claim.
     */
    private static final String USERNAME = "userName";
    /**
     * Logger {@link Logger} instance.
     */
    private final Logger logger;
    /**
     * {@code BCrypt} encoder instance.
     */
    private final BCryptPasswordEncoder encoder;
    /**
     * HMAC SHA key for the JWT token.
     */
    @Value("${my.hmacSHAkey}")
    private String hmacSHAKey;

    @Autowired
    public UserController(UserRepo repo, BCryptPasswordEncoder encoder, Logger logger) {
        super(repo);
        this.encoder = encoder;
        this.logger = logger;
    }

    @PostMapping(path = "/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<User>> save(@RequestBody User user) {
        Objects.requireNonNull(user, "Request body is null");

        var response = user.response(UserValidation.class);
        if (response.getValidation() != null && !response.getValidation().isEmpty()) {
            return ResponseEntity.ok(response);
        }

        var body = response.getBody();
        body.setPassword(encoder.encode(user.getPassword()));
        body.setUserName(appendDomain(body.getUserName()));

        repository.save(body);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }

    @PostMapping("/login")
    public ResponseEntity<Void> login(@NotNull @ModelAttribute Login login) {
        if (login.getEmail() != null) {
            Optional<User> optionalUser = ((UserRepo) repository).findOptionalByUserName(login.getEmail().toLowerCase());
            if (optionalUser.isPresent()
                    && encoder.matches(login.getPassword(), optionalUser.get().getPassword())) {

                final User user = optionalUser.get();
                logger.info(() -> String.format("Access granted for the user -> %s", user.getUserName()));

                String token = Jwts
                        .builder()
                        .setClaims(Map.of(
                                ID, user.getId(),
                                USERNAME, user.getUserName()))
                        .setExpiration(expirationDate())
                        .signWith(Keys.hmacShaKeyFor(hmacSHAKey.getBytes(StandardCharsets.UTF_8)))
                        .compact();

                HttpCookie tokenCookie = ResponseCookie
                        .from(SECURITY_TOKEN.getName(), token)
                        .path(SECURITY_TOKEN.getPath())
                        .httpOnly(true)
                        .maxAge(Duration.ofMinutes(COOKIE_MAX_AGE))
                        .build();
                return ResponseEntity
                        .ok()
                        .header(HttpHeaders.SET_COOKIE, tokenCookie.toString())
                        .build();
            }
        }

        logger.info(() -> String.format("Access denied for the user -> %s", login.getEmail()));
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .build();
    }

    @PostMapping("/logout")
    public ResponseEntity<Void> logout(@NotNull HttpServletRequest request) {
        final Optional<String> optionalToken = Optional.ofNullable(request.getHeader(X_AUTHORIZATION.getHeaderName()));
        if (optionalToken.isPresent() && StringUtils.isNotEmpty(optionalToken.get())) {
            return ResponseEntity
                    .status(HttpStatus.RESET_CONTENT)
                    .header(HttpHeaders.SET_COOKIE,
                            ResponseCookie
                                    .from(SECURITY_TOKEN.getName(), StringUtils.EMPTY)
                                    .path(SECURITY_TOKEN.getPath())
                                    .httpOnly(true)
                                    .maxAge(0)
                                    .build()
                                    .toString())
                    .build();
        }
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .build();
    }

    @PostMapping("/validate-cookie")
    public ResponseEntity<Void> validateToken(@NotNull HttpServletRequest request) {
        final Optional<String> optionalToken = Optional.ofNullable(request.getHeader(X_AUTHORIZATION.getHeaderName()));
        if (optionalToken.isPresent() && StringUtils.isNotEmpty(optionalToken.get())) {
            try {
                Jws<Claims> jws = Jwts.parser()
                        .setSigningKey(Keys.hmacShaKeyFor(hmacSHAKey.getBytes(StandardCharsets.UTF_8)))
                        .parseClaimsJws(optionalToken.get());

                final String userName = jws.getBody().get(USERNAME, String.class);
                if (((UserRepo) repository).isUserAvailable(userName)) {
                    logger.info(() -> String.format("Request authorized for the user -> %s", userName));
                    return ResponseEntity
                            .ok()
                            .build();
                }
            } catch (ExpiredJwtException ex) {
                logger.error(() -> String.format("Expired token -> %s", optionalToken.get()), () -> ex);
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PostMapping("/refresh-cookie")
    public ResponseEntity<Token> refreshToken(@NotNull HttpServletRequest request) {
        final Optional<String> optionalToken = Optional.ofNullable(request.getHeader(X_AUTHORIZATION.getHeaderName()));
        if (optionalToken.isPresent() && StringUtils.isNotEmpty(optionalToken.get())) {
            Jws<Claims> jws = Jwts.parser()
                    .setSigningKey(Keys.hmacShaKeyFor(hmacSHAKey.getBytes(StandardCharsets.UTF_8)))
                    .parseClaimsJws(optionalToken.get());

            logger.info(() -> String.format("Request cookie refreshed for signature -> %s", jws.getSignature()));
            final String validatedToken = Jwts.builder().setClaims(jws.getBody())
                    .setExpiration(expirationDate())
                    .signWith(Keys.hmacShaKeyFor(hmacSHAKey.getBytes(StandardCharsets.UTF_8)))
                    .compact();
            return ResponseEntity
                    .ok(new Token(validatedToken));
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Response<User>> update(@PathVariable("id") String id, @RequestBody final User user) {
        Objects.requireNonNull(user, "Request body is null");
        return super.update(() -> repository.findById(id).map(oldUser -> {
            user.setId(id);
            user.setCreateDateTme(oldUser.getCreateDateTme());

            if (StringUtils.isEmpty(user.getPassword())) {
                user.setPassword(oldUser.getPassword());
            } else {
                user.setPassword(encoder.encode(user.getPassword()));
            }
            return user;
        }).orElse(null));
    }

    @GetMapping("/all")
    public ResponseEntity<Response<List<User>>> findAll() {
        return super.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<User>> findById(@PathVariable("id") String id) {
        return super.findById(id);
    }

    @GetMapping("/exists/{userName}")
    public ResponseEntity<Response<User>> isUserAvailableByUserName(@PathVariable("userName") String userName) {
        boolean isAvailable = ((UserRepo) repository).isUserAvailable(appendDomain(userName));
        if (isAvailable) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response<User>> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }

    /**
     * Get the new expiration {@link Date} instance.
     *
     * @return new {@link Date} object with plus with {@code COOKIE_MAX_AGE}
     */
    @NotNull
    private Date expirationDate() {
        return Date.from(LocalDateTime.now().plusMinutes(COOKIE_MAX_AGE)
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}