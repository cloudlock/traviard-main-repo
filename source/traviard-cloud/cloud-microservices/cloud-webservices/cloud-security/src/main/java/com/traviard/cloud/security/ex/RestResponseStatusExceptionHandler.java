package com.traviard.cloud.security.ex;

import com.traviard.cloud.exhandler.GlobalExceptionHandler;
import com.traviard.cloud.exhandler.util.HttpErrorResponse;
import com.traviard.cloud.log4j.Logger;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sachith Dickwella
 */
@ControllerAdvice
public class RestResponseStatusExceptionHandler extends GlobalExceptionHandler {

    /**
     * Autowiring instances to the constructor.
     *
     * @param logger {@link Logger} instance
     */
    @Autowired
    public RestResponseStatusExceptionHandler(Logger logger) {
        super(logger);
    }

    /**
     * Global exception handler for service RuntimeExceptions.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({RuntimeException.class})
    @Override
    public ResponseEntity<HttpErrorResponse> handleRuntimeException(HttpServletRequest request, RuntimeException ex) {
        return super.handleRuntimeException(request, ex);
    }

    /**
     * NullPointer exception global handler.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({NullPointerException.class})
    @Override
    public ResponseEntity<HttpErrorResponse> handleNullPointerException(HttpServletRequest request, NullPointerException ex) {
        return super.handleNullPointerException(request, ex);
    }

    /**
     * {@link JwtException} instance hanler.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link JwtException} instance
     * @return Instance of {@link ResponseEntity}
     */
    @ExceptionHandler({JwtException.class})
    public ResponseEntity<HttpErrorResponse> handleJwtException(HttpServletRequest request, JwtException ex) {
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        logger.error(status::getReasonPhrase, () -> ex);
        return getResponseEntity(request, status);
    }
}
