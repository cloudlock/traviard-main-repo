package com.traviard.cloud.security.controller;

import com.traviard.cloud.facade.CrudRepositoryAbstractController;
import com.traviard.cloud.facade.Response;
import com.traviard.cloud.security.entity.Country;
import com.traviard.cloud.security.entity.repo.CountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Sachith Dickwella
 */
@RestController
@RequestMapping("/country")
public class CountryController extends CrudRepositoryAbstractController<Country, String> {

    @Autowired
    public CountryController(CountryRepo repo) {
        super(repo);
    }

    @PostMapping("/create")
    public ResponseEntity<Response<Country>> save(@RequestBody Country country) {
        return super.save(country);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Response<Country>> update(@PathVariable("id") String id, @RequestBody Country country) {
        return super.update(() -> {
            country.setId(id);
            return country;
        });
    }

    @GetMapping("/all")
    public ResponseEntity<Response<List<Country>>> findAll() {
        return super.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response<Country>> findById(@PathVariable("id") String id) {
        return super.findById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response<Country>> delete(@PathVariable("id") String id) {
        return super.delete(id);
    }
}
