package com.traviard.cloud.security.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;

/**
 * @author Sachith Dickwella
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /*
     * CSRF cookie token repository.
     */
    private CsrfTokenRepository csrfTokenRepository;

    /**
     * Single-arg autowired constructor for the class which invoke by
     * the bean container.
     */
    @Autowired
    public WebSecurityConfig(@Qualifier("csrfTokenRepo") CsrfTokenRepository csrfTokenRepository) {
        this.csrfTokenRepository = csrfTokenRepository;
    }

    @Override
    protected void configure(@NotNull HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**")
                .permitAll()
                .and()
                .csrf()
                .csrfTokenRepository(this.csrfTokenRepository)
                .and()
                .httpBasic();
    }

    /**
     * When password encryption, value type need to be provided.
     * In order to do that, bind String class as an injectable
     * object.
     *
     * @return Instance of {@link BCryptPasswordEncoder}
     */
    @Bean
    public BCryptPasswordEncoder getBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
