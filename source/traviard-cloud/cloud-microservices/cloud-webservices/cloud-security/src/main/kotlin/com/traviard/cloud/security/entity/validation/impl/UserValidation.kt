package com.traviard.cloud.security.entity.validation.impl

import com.traviard.cloud.security.entity.repo.CompanyRepo
import com.traviard.cloud.security.entity.repo.CountryRepo
import com.traviard.cloud.security.entity.repo.UserRepo
import com.traviard.cloud.util.ApplicationConstants.appendDomain
import com.traviard.cloud.util.BeanUtils.getBean
import com.traviard.cloud.util.OWASPInputValidator.Companion.isEmpty
import com.traviard.cloud.util.OWASPInputValidator.Companion.isISOBasicLatin
import com.traviard.cloud.util.OWASPInputValidator.Companion.isNotTooLong
import com.traviard.cloud.util.OWASPInputValidator.Companion.isNotTooShort
import com.traviard.cloud.util.OWASPInputValidator.Companion.isNull
import com.traviard.cloud.util.OWASPInputValidator.Companion.isValidDOB
import com.traviard.cloud.util.OWASPInputValidator.Companion.isValidEmailFormat
import com.traviard.cloud.util.OWASPInputValidator.Companion.isValidMobileNumber
import com.traviard.cloud.util.OWASPInputValidator.Companion.isValidUserNameFormat
import com.traviard.cloud.util.Tuple
import com.traviard.cloud.validation.Validatable
import org.bson.types.Binary
import java.time.LocalDate

/**
 * This class and its sub-classes are reflectively resolve and execute to validate the
 * [com.traviard.cloud.security.entity.User] entity type value that come over HTTP in JSON.
 * Each subclass in this class represents a member field in a [com.traviard.cloud.security.entity.User]
 * instance that coming from client's end. That's the main reason this particular entity
 * need to be thoroughly validated.
 *
 * All the classes that required validation, are extended with [Validatable] interface to
 * main dynamic validation mechanism.
 *
 * Decorate with [Suppress] annotation with unchecked value, even though all the classes being
 * used by the application, compiler doesn't recognize them.
 *
 * @author Sachith Dickwella
 */
@Suppress("unused")
interface UserValidation<T> : Validatable<T> {

    class FirstName : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                isEmpty(input) -> Tuple(javaClass.simpleName, "First name is required")
                !isISOBasicLatin(input!!) -> Tuple(javaClass.simpleName, "Only letters allowed")
                !isNotTooLong(input, 20) -> Tuple(javaClass.simpleName, "First name is too long")
                else -> null
            }
        }
    }

    class LastName : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                isEmpty(input) -> Tuple(javaClass.simpleName, "Last name is required")
                !isISOBasicLatin(input!!) -> Tuple(javaClass.simpleName, "Only letters allowed")
                !isNotTooLong(input, 20) -> Tuple(javaClass.simpleName, "Last name is too long")
                else -> null
            }
        }
    }

    class UserName : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                isEmpty(input) || !isValidUserNameFormat(input!!) ->
                    Tuple(javaClass.simpleName, "Letters, numbers, periods, hypens and underscores only")
                !isNotTooLong(input, 50) ->
                    Tuple(javaClass.simpleName, "Username is too long")
                !isNotTooShort(input, 6) ->
                    Tuple(javaClass.simpleName, "Username should at least 6 letters long")
                getBean(UserRepo::class.java).isUserAvailable(appendDomain(input)) ->
                    Tuple(javaClass.simpleName, "Username is not available")
                else -> null
            }
        }
    }

    class AltEmail : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                isEmpty(input) -> Tuple(javaClass.simpleName, "Alternative email is required to recover login info")
                !isNotTooLong(input!!, 60) -> Tuple(javaClass.simpleName, "Alternative email is too long")
                !isValidEmailFormat(input) -> Tuple(javaClass.simpleName, "Valid email 'john@example.com'")
                else -> null
            }
        }
    }

    class Password : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                isEmpty(input) || !isNotTooShort(input!!, 6) ->
                    Tuple(javaClass.simpleName, "Minimum 6 characters")
                else -> null
            }
        }
    }

    class MobileNumber : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                !isEmpty(input) ->
                    if (isNotTooShort(input!!, 8) && !isValidMobileNumber(input))
                        Tuple(javaClass.simpleName, "Invalid phone number")
                    else if (!isNotTooShort(input, 8))
                        Tuple(javaClass.simpleName, "Phone number is too short")
                    else null
                else -> null
            }
        }
    }

    class DateOfBirth : UserValidation<LocalDate> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [LocalDate] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: LocalDate?): Tuple<String, String>? {
            return when {
                isNull(input) -> Tuple(javaClass.simpleName, "Date of birth is required")
                isValidDOB(input ?: LocalDate.now()) ->
                    Tuple(javaClass.simpleName, "Are you sure is this your birth day?")
                else -> null
            }
        }
    }

    class Gender : UserValidation<String> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [String] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: String?): Tuple<String, String>? {
            return when {
                !isEmpty(input) ->
                    if (!(input.equals("Male") || input.equals("Female")))
                        Tuple(javaClass.simpleName, "Not a gender")
                    else null
                else -> null
            }
        }
    }

    class ProfilePicture : UserValidation<Binary> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [Binary] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: Binary?): Tuple<String, String>? {
            return null
        }
    }

    class Country : UserValidation<com.traviard.cloud.security.entity.Country> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [com.traviard.cloud.security.entity.Country] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: com.traviard.cloud.security.entity.Country?): Tuple<String, String>? {
            return when {
                isNull(input) -> Tuple(javaClass.simpleName, "Country is missing")
                !getBean(CountryRepo::class.java).isAvailable(input!!.id) ->
                    Tuple(javaClass.simpleName, "Invalid country")
                else -> null
            }
        }
    }

    class Company : UserValidation<com.traviard.cloud.security.entity.Company> {

        /**
         * Execute the implementation and return te result reference [Tuple] instance.
         *
         * @param input [com.traviard.cloud.security.entity.Company] type input to validate
         * @return a [Tuple] result instance
         */
        override fun validate(input: com.traviard.cloud.security.entity.Company?): Tuple<String, String>? {
            return when {
                !isNull(input) && !getBean(CompanyRepo::class.java).isAvailable(input!!.id) ->
                    Tuple(javaClass.simpleName, "Invalid company")
                else -> null
            }
        }
    }
}