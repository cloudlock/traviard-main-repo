package com.traviard.cloud.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.traviard.cloud.exhandler.exception.InstantiationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Common entity operations define in this interface.
 *
 * @author Sachith Dickwella
 */
public abstract class EntityUtils<T> {

    /**
     * Create empty object when the builder pattern not present.
     *
     * @return New instance of {@link T} type
     */
    @NotNull
    @SuppressWarnings("unused")
    public final T empty(@NotNull Class<T> classT) {
        try {
            return classT.getConstructor().newInstance();
        } catch (IllegalAccessException
                | NoSuchMethodException
                | java.lang.InstantiationException
                | InvocationTargetException ex) {

            throw new InstantiationException("", ex); // TODO fill the string.
        }
    }

    /**
     * Marshall given type instance into a Json format String.
     *
     * @return Stringify JSON object
     */
    public static <T> String toJson(T object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            throw new InstantiationException(ex);
        }
    }

    /**
     * Un-marshall the given stringify JSON array object to Java generic {@link List} object.
     *
     * @param json      Stringify JSON object
     * @param classType {@link Class} of the List's generic type
     * @return Un-marshall JSON object into a type-safe {@link List}
     */
    @SuppressWarnings("unused")
    public static <T> List<T> unmarshallerToList(String json, Class<T> classType) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            CollectionType javaType = mapper.getTypeFactory()
                    .constructCollectionType(List.class, classType);

            return mapper.readValue(json, javaType);
        } catch (IOException ex) {
            throw new InstantiationException(ex);
        }
    }
}
