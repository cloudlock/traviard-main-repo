package com.traviard.cloud.facade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.traviard.cloud.util.Tuple;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

/**
 * @author Sachith Dickwella
 */
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response<T> {

    /**
     * Actual object came with the request.
     */
    private T body;
    /**
     * Validation results {@link Map}.
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<Tuple<String, String>> validation;

    /**
     * No-args constructor for empty responses.
     */
    private Response() {
        /*
         * Nothing goes here.
         */
    }

    /**
     * Single-args constructor to ignore validation result for default implementations.
     *
     * @param body Type {@link T} reference
     */
    private Response(@NotNull T body) {
        this();
        this.body = body;
    }

    /**
     * Single-args constructor to initialize {@link T} type reference.
     *
     * @param body       Type {@link T} reference
     * @param validation {@link Map} of validation results
     */
    private Response(@NotNull T body, @NotNull Set<Tuple<String, String>> validation) {
        this(body);
        this.validation = validation;
    }

    /**
     * Return an empty instance of {@link Response} for empty responses.
     *
     * @return an empty {@link Response} instance
     */
    @SuppressWarnings("unused")
    @NotNull
    static <T> Response<T> newInstance() {
        return new Response<>();
    }

    /**
     * Create new instance of {@link Response} class with single argument and return.
     * Mostly uses for default implementations of own class.
     *
     * @param body {@link T} type instance
     * @return new {@link Response} instance
     */
    @NotNull
    static <T> Response<T> newInstance(@NotNull T body) {
        return new Response<>(body);
    }

    /**
     * Create new instance of {@link Response} class with two arguments and return.
     *
     * @param body    {@link T} type instance
     * @param results {@link Map} of validation results and messages if any available
     * @return new {@link Response} instance
     */
    @NotNull
    static <T> Response<T> newInstance(@NotNull T body, @NotNull Set<Tuple<String, String>> results) {
        return new Response<>(body, results);
    }
}
