package com.traviard.cloud.facade;

import com.traviard.cloud.repository.CommonRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author Sachith Dickwella
 */
public abstract class CrudRepositoryAbstractController<T, K> implements AbstractController {

    protected CrudRepository<T, K> repository;

    protected CrudRepositoryAbstractController(CrudRepository<T, K> repository) {
        this.repository = repository;
    }

    /**
     * Create new entry in the database.
     *
     * @param entity Database entity instance
     * @return Instance of entity object wrapped with {@link ResponseEntity}
     */
    protected ResponseEntity<Response<T>> save(T entity) {
        return ResponseEntity.ok(Response.newInstance(repository.save(entity)));
    }

    /**
     * Modify/Update the entry in the database.
     *
     * @param modifiable Instance of {@link Modifiable} interface which contains process the modification on the
     *                   object in the database base on the given object.
     * @return Modified and saved object of particular entity
     */
    protected ResponseEntity<Response<T>> update(@NotNull Modifiable<T> modifiable) {
        T modified = modifiable.modify();
        if (modified == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(Response.newInstance(repository.save(modified)));
    }

    /**
     * Fetch all the entries of the selected entity.
     *
     * @return {@link List} of entries wrapped with {@link ResponseEntity} instance
     */
    protected ResponseEntity<Response<List<T>>> findAll() {
        List<T> all = (List<T>) repository.findAll();
        if (all.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(Response.newInstance(all));
    }

    /**
     * Fetch the specific entry given id.
     *
     * @param id {@link K} type id
     * @return Specific entity instance
     */
    public ResponseEntity<Response<T>> findById(K id) {
        return repository.findById(id)
                .map(res -> ResponseEntity.ok(Response.newInstance(res)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Delete a entity from the database by given id.
     *
     * @param id {@link K} type id
     * @return Deleted entity won't return, Instead, function returns a {@link ResponseEntity}
     * instance with 204 or 404 status.
     */
    @SuppressWarnings("unchecked")
    public ResponseEntity<Response<T>> delete(K id) {
        boolean isAvailable = ((CommonRepository<K>) repository).isAvailable(id);
        if (isAvailable) {
            repository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
