package com.traviard.cloud.facade;

import org.springframework.stereotype.Indexed;

/**
 * @author Sachith Dickwella
 */
@Indexed
interface AbstractController {
    /*
     * Markup interface for now and should be a public interface.
     */
}
