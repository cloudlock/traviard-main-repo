package com.traviard.cloud.facade;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.traviard.cloud.util.StringUtils;
import com.traviard.cloud.util.Tuple;
import com.traviard.cloud.validation.SkipValidation;
import com.traviard.cloud.validation.Validatable;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Transient;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sachith Dickwella
 */
public abstract class Entity<T> {

    /**
     * Entity class type.
     */
    @JsonIgnore
    @Transient
    private Class<T> tClass;

    /**
     * Single args constructor to capture original entity class.
     *
     * @param tClass Sub-class type
     */
    public Entity(@NotNull Class<T> tClass) {
        this.tClass = tClass;
    }

    /**
     * Get the {@link T} type class's declared member field names and {@link Tuple} it
     * and execute the parameterized class's subclasses into the validation by invoking
     * the {@code validate} function.
     *
     * @param vClass {@link Validatable} class' subclass to validate its subclasses that
     *               represent member attribute of {@link #tClass}.
     * @return empty {@link Response} to be filled as required
     */
    @SuppressWarnings("unchecked")
    @NotNull
    @JsonIgnore
    @Transient
    public final Response<T> response(@SuppressWarnings("rawtypes") @NotNull Class<? extends Validatable> vClass) {
        final var me = tClass.cast(this);
        final var fields = Stream.of(tClass.getDeclaredFields())
                .filter(field -> !field.isAnnotationPresent(SkipValidation.class))
                .map(Field::getName)
                .filter(fName -> !fName.equals("id"))
                .collect(Collectors.toSet());

        final var tuples = Stream.of(vClass.getDeclaredClasses())
                .filter(c -> fields.contains(StringUtils.miniscule(c.getSimpleName())))
                .map(c -> {
                    try {
                        var field = tClass.getDeclaredField(StringUtils.miniscule(c.getSimpleName()));
                        field.setAccessible(true);

                        var entry = c.getMethod("validate", field.getType())
                                .invoke(c.getDeclaredConstructor().newInstance(), field.getType().cast(field.get(me)));

                        if (entry != null) return (Tuple<String, String>) entry;
                        return null;
                    } catch (NoSuchMethodException | InstantiationException | NoSuchFieldException
                            | IllegalAccessException | InvocationTargetException ex) {
                        throw new RuntimeException(ex);
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        return Response.newInstance(me, tuples);
    }
}
