package com.traviard.cloud.repository;

/**
 * @author Sachith Dickwella
 */
@FunctionalInterface
public interface CommonRepository<T> {

    /**
     * Check if the entity represent by the parameter {@code id} is available in the database.
     *
     * @param id entity id
     * @return the availability status of the entity.
     */
    Boolean isAvailable(T id);
}
