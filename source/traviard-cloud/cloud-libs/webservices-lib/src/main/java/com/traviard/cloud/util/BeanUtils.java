package com.traviard.cloud.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * @author Sachith Dickwella
 */
@Service
public class BeanUtils implements ApplicationContextAware {

    private static ApplicationContext ctx;

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

    /**
     * Return a new instance of the class passed by the parameter if only the
     * parameterized class bound as a bean to the Spring context.
     *
     * @param tClass Spring bean {@link Class<T>} instance
     * @return instance of the bound class to the Spring context
     */
    @NotNull
    public static <T> T getBean(@NotNull Class<T> tClass) {
        return ctx.getBean(tClass);
    }
}
