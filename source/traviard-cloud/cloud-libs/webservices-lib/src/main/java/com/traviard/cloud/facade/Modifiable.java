package com.traviard.cloud.facade;

/**
 * @author Sachith Dickwella
 */
@FunctionalInterface
public interface Modifiable<T> {

    /**
     * Execute the function and return the modified
     * object.
     *
     * @return Modified object
     */
    T modify();
}
