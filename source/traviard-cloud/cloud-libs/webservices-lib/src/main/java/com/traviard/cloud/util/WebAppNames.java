package com.traviard.cloud.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;

import static com.traviard.cloud.util.ApplicationConstants.QueryParamNames.APPLICATION_ID_PARAM;

/**
 * @author Sachith Dickwella
 */
@Getter
@AllArgsConstructor
public enum WebAppNames {

    DATA_STORE("c6595f15-4aa8-48bb-b532-7b8a49a53295", "Data Store"),
    E_COMMERCE("78638b2d-aa4b-4019-a8a5-bc7cbf6ddd12", "Shopping Store");

    /**
     * Application id {@link String} value.
     */
    private String id;
    /**
     * Application {@link String} name.
     */
    private String name;

    /**
     * Return application name as defined in {@link WebAppNames} enum by the parameter
     * {@code n} on the request URL.
     *
     * @param request {@link HttpServletRequest} instance
     * @return {@link String} value of application name
     */
    @NotNull
    public static String getAppName(@NotNull HttpServletRequest request) {
        var n = request.getParameter(APPLICATION_ID_PARAM.getQueryParamName());
        if (!StringUtils.isEmpty(n)) {
            return Stream.of(WebAppNames.values())
                    .filter(e -> e.getId().equals(n))
                    .map(WebAppNames::getName)
                    .map(StringUtils.EMPTY_SPACE::concat)
                    .findFirst()
                    .orElse(StringUtils.EMPTY);
        }
        return StringUtils.EMPTY;
    }
}
