package com.traviard.cloud.validation;

import com.traviard.cloud.util.Tuple;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * Subclasses of this {@link Validatable} interface needs no-args constructors to work properly.
 *
 * @author Sachith Dickwella
 */
@FunctionalInterface
public interface Validatable<T> {

    /**
     * Execute the implementation and return te result reference {@link Map.Entry} instance.
     *
     * @param input {@link T} type POJO to validate
     * @return a {@link Map.Entry} result instance
     */
    @Nullable
    Tuple<String, String> validate(@Nullable T input);
}
