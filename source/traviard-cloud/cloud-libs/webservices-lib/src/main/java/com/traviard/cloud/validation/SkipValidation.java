package com.traviard.cloud.validation;

import java.lang.annotation.*;

/**
 * @author Sachith Dickwella
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SkipValidation {
    /*
     * Mark up annotation to mark down to skip validation for fields in entity classes.
     *
     * When mark for skip validation in {@link Entity} classes, simply ignore the {@code id}
     * field by name if not marked by default. Using {@link SkipValidation} on {@code id}
     * fields don't make any different unless the fields value is different. If so, should have
     * {@link Validatable} class implementation of that particular field in the entity or fails
     * with {@link ClassNotFoundException}.
     */
}
