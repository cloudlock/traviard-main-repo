package com.traviard.cloud.util

import java.time.LocalDate

/**
 * @author Sachith Dickwella
 */
abstract class OWASPInputValidator {

    companion object {

        /**
         * Check if the [input] value is null and if so return true to upstream.
         *
         * @param input [Any] to validate null
         * @return 'true' if the [input] value is null
         */
        fun isNull(input: Any?): Boolean = input == null

        /**
         * Check if the [input] value is not null nor empty.
         *
         * @param input [String] to validate null and empty values
         * @return 'true' if the [input] value is null or empty
         */
        @JvmStatic
        fun isEmpty(input: String?): Boolean = input == null || input.isEmpty()

        /**
         * Matches the [input] parameter value against [a-zA-Z] regular expression.
         *
         * @param input [String] to validate against regex
         * @return the matching status of the input
         */
        @JvmStatic
        fun isISOBasicLatin(input: String): Boolean = Regex("[a-zA-Z]+").matches(input)

        /**
         * Check the [input] parameter length requirement against the [length] parameter.
         *
         * @param input [String] to validate against the [length] parameter
         * @param length character count to check against input value length
         * @return if satisfy the length requirement
         */
        @JvmStatic
        fun isNotTooLong(input: String, length: Int): Boolean = input.length <= length

        /**
         * Check the [input] parameter length requirement against the [length] parameter.
         *
         * @param input [String] to validate against the [length] parameter
         * @param length character count to check against input value length
         * @return if satisfy the length requirement
         */
        @JvmStatic
        fun isNotTooShort(input: String, length: Int): Boolean = input.length >= length

        /**
         * Check the validity of the [input] parameter value.
         *
         * @param input [String] to validate against regex
         * @return the matching status of the input
         */
        @JvmStatic
        fun isValidEmailFormat(input: String): Boolean = Regex("^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+\$")
                .matches(input)

        /**
         * Check the validity of [input] field's username value against the regular expression.
         *
         * @param input [String] to validate against regex
         * @return the matching status of the input
         */
        @JvmStatic
        fun isValidUserNameFormat(input: String): Boolean = Regex("^([a-zA-Z0-9_.+-])+\$").matches(input)

        /**
         * Check the validity of [input] field's mobile number value against the regular expression.
         *
         * @param input [String] to validate against regex
         * @return the matching status of the input
         */
        @JvmStatic
        fun isValidMobileNumber(input: String): Boolean = Regex("^(\\+)([0-9]{8,15})\$").matches(input)

        /**
         * Check if provided [LocalDate] instance could be a valid date of birth comparing to
         * current system date.
         *
         * @param input instance of [LocalDate] with DOB
         * @return the valid instance of the [LocalDate]
         */
        @JvmStatic
        fun isValidDOB(input: LocalDate): Boolean = !(input.year >= 1950 && input.year <= (LocalDate.now().year - 10))
    }
}