package com.traviard.cloud.log4j.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * Template of the logging message that created from application
 * events. This includes critical information to trace an event
 * from the ground up.
 *
 * @author Sachith Dickwella
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Message {

    /**
     * Log message UUID id.
     */
    private final UUID id;
    /**
     * Log message entry date and time.
     */
    private LocalDateTime dateTime;
    /**
     * Log message event's level.
     */
    private Level level;
    /**
     * Log message event's thread name.
     */
    private String thread;
    /**
     * Log message event's class name.
     */
    @JsonProperty("class")
    private Class<?> aClass;
    /**
     * Event type that indicate where the message originated from.
     */
    private Type type;
    /**
     * {@link String} message name where message originated from.
     */
    private String method;
    /**
     * Execution line of the code.
     */
    private Integer line;
    /**
     * {@link String} descriptive message.
     */
    @JsonProperty("message")
    private String description;
    /**
     * {@link String} application
     */
    private String applicationName;
    /**
     * {@link Throwable} on an error.
     */
    private Throwable throwable;

    /**
     * Private constructor to avoid object creation outside the
     * class and initialize the {@link UUID}, {@link #id} instance.
     */
    private Message() {
        this.id = UUID.randomUUID();
    }

    /**
     * Initiate a new {@link Build} instance and return.
     *
     * @return new instance of {@link Build} class.
     */
    @NotNull
    public static Build builder() {
        return new Build();
    }

    public static class Build {

        /**
         * Log {@link Message} instance to be built.
         */
        private final Message message;

        /**
         * Initiate {@link Build}er process with the constructor.
         */
        private Build() {
            this.message = new Message();
        }

        /**
         * Set the value {@code dateTime} of {@link LocalDateTime}.
         *
         * @param dateTime value of {@link LocalDateTime} type.
         * @return a instance of {@link Build}.
         */
        public Build dateTime(LocalDateTime dateTime) {
            this.message.dateTime = dateTime;
            return this;
        }

        /**
         * Set the value {@code level} of {@link Level}.
         *
         * @param level value of {@link Level} type.
         * @return a instance of {@link Build}.
         */
        public Build level(Level level) {
            this.message.level = level;
            return this;
        }

        /**
         * Set the value {@code thread} of {@link String}.
         *
         * @param thread value of {@link String} type.
         * @return a instance of {@link Build}.
         */
        public Build thread(String thread) {
            this.message.thread = thread;
            return this;
        }

        /**
         * Set the value {@code aClass} of {@link Class}.
         *
         * @param aClass value of {@link Class} type.
         * @return a instance of {@link Build}.
         */
        public Build clazz(Class<?> aClass) {
            this.message.aClass = aClass;
            return this;
        }

        /**
         * Set the value {@code type} of {@link Type}.
         *
         * @param type value of {@link Type} type.
         * @return a instance of {@link Build}.
         */
        public Build type(Type type) {
            this.message.type = type;
            return this;
        }

        /**
         * Set the value {@code method} of {@link String}.
         *
         * @param method value of {@link String} type.
         * @return a instance of {@link Build}.
         */
        public Build method(String method) {
            this.message.method = method;
            return this;
        }

        /**
         * Set the value {@code line} of {@link Integer}.
         *
         * @param line value of {@link Integer} type.
         * @return a instance of {@link Build}.
         */
        public Build line(Integer line) {
            this.message.line = line;
            return this;
        }

        /**
         * Set the value {@code message} of {@link String}.
         *
         * @param supplier value of {@link Supplier} type.
         * @return a instance of {@link Build}.
         */
        public Build description(Supplier<String> supplier) {
            this.message.description = supplier.get();
            return this;
        }

        /**
         * Set the value {@code applicationName} of {@link String}.
         *
         * @param applicationName value of {@link String} type.
         * @return a instance of {@link Build}.
         */
        public Build applicationName(String applicationName) {
            this.message.applicationName = applicationName;
            return this;
        }

        /**
         * Set the value {@code throwable} of {@link Throwable}.
         *
         * @param supplier value of {@link Throwable} type.
         * @return a instance of {@link Build}.
         */
        public Build throwable(@NotNull Supplier<? extends Throwable> supplier) {
            this.message.throwable = supplier.get();
            return this;
        }

        /**
         * Build and return the built {@link #message} instance.
         *
         * @return the built {@link Message}.
         */
        public Message build() {
            return this.message;
        }
    }

    /**
     * Get the value {@code id} of {@link UUID}.
     *
     * @return the value of {@link #id} field.
     */
    public UUID id() {
        return id;
    }

    /**
     * Get the value {@code dateTime} of {@link LocalDateTime}.
     *
     * @return the value of {@link #dateTime} field.
     */
    public LocalDateTime dateTime() {
        return dateTime;
    }

    /**
     * Get the value {@code level} of {@link Level}.
     *
     * @return the value of {@link #level} field.
     */
    public Level level() {
        return level;
    }

    /**
     * Get the value {@code thread} of {@link String}.
     *
     * @return the value of {@link #thread} field.
     */
    public String thread() {
        return thread;
    }

    /**
     * Get the value {@code aClass} of {@link Class}.
     *
     * @return the value of {@link #aClass} field.
     */
    public Class<?> clazz() {
        return aClass;
    }

    /**
     * Get the value {@code type} of {@link Type}.
     *
     * @return the value of {@link #type} field.
     */
    public Type type() {
        return type;
    }

    /**
     * Get the value {@code method} of {@link String}.
     *
     * @return the value of {@link #method} field.
     */
    public String method() {
        return method;
    }

    /**
     * Get the value {@code line} of {@link Integer}.
     *
     * @return the value of {@link #line} field.
     */
    public Integer line() {
        return line;
    }

    /**
     * Get the value {@code message} of {@link String}.
     *
     * @return the value of {@link #description} field.
     */
    public String description() {
        return description;
    }

    /**
     * Get the value {@code applicationName} of {@link String}.
     *
     * @return the value of {@link #applicationName} field.
     */
    public String applicationName() {
        return applicationName;
    }

    /**
     * Get the value {@code throwable} of {@link Throwable}.
     *
     * @return the value of {@link #throwable} field.
     */
    public Throwable throwable() {
        return throwable;
    }
}
