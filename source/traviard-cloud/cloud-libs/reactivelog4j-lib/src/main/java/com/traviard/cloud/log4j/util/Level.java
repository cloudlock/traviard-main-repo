package com.traviard.cloud.log4j.util;

/**
 * @author Sachith Dickwella
 */
public enum Level {

    /**
     * All level of log entries.
     */
    ALL,
    /**
     * Trace level of log entries with start and end times.
     */
    TRACE,
    /**
     * Information level of log entries with regular events.
     */
    INFO,
    /**
     * Debug level of log entries to use on developer traces.
     */
    DEBUG,
    /**
     * Warning level of log entries with regular events.
     */
    WARN,
    /**
     * Error level of log entries with {@link Throwable} object.
     */
    ERROR,
    /**
     * Fatal level of log entries where application terminates.
     */
    FATAL;
}
