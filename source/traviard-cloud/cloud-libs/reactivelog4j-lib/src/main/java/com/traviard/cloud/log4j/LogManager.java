package com.traviard.cloud.log4j;

import com.traviard.cloud.log4j.appender.Appender;
import com.traviard.cloud.log4j.util.Level;
import com.traviard.cloud.log4j.util.Message;
import com.traviard.cloud.log4j.util.Type;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContextException;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Sachith Dickwella
 */
public abstract class LogManager implements Logger, ApplicationContextAware {

    /**
     * Spring {@link ApplicationContext} reference instance to get bean
     * information.
     */
    private static ApplicationContext appContext;
    /**
     * Application name that initiated logging.
     */
    private final String appName;
    /**
     * {@link Type} of the logging initiated.
     */
    private final Type type;
    /**
     * Appender instance to append the message to downstream target.
     */
    private final Appender appender;

    /**
     * Constructor to initialize final {@link #appender} instance from
     * the sub-class.
     *
     * @param appender to append messages to downstream target.
     * @param appName  of the module that logging started.
     * @param type     of the logging to distinguish where entry originated
     *                 from.
     */
    public LogManager(Appender appender, String appName, Type type) {
        this.appender = appender;
        this.appName = appName;
        this.type = type;
    }

    /**
     * Get the {@link StackWalker.StackFrame} stream limit that
     * needs to get the call class.
     *
     * @return the {@link StackWalker.StackFrame} stream limit
     * as an {@code int} value.
     */
    @Override
    public int stackFrameLimit() {
        return 1;
    }

    /**
     * Set the ApplicationContext that this object runs in. Normally this call will
     * be used to initialize the object. Invoked after population of normal bean
     * properties but before an init callback such as {@link InitializingBean#afterPropertiesSet()}
     * or a custom init-method.
     * <p>
     * Invoked after {@link org.springframework.context.ResourceLoaderAware#setResourceLoader},
     * {@link org.springframework.context.ApplicationEventPublisherAware#setApplicationEventPublisher}
     * and {@link org.springframework.context.MessageSourceAware}, if applicable.
     *
     * @param appContext the ApplicationContext object to be used by this object
     * @throws ApplicationContextException in case of context initialization errors
     * @throws BeansException              if thrown by application context methods
     * @see BeanInitializationException
     */
    @SuppressWarnings("java:S2696")
    @Override
    public void setApplicationContext(@NotNull ApplicationContext appContext) {
        LogManager.appContext = appContext;
    }

    /**
     * Log information messages.
     *
     * @param message {@link String} instance.
     */
    public void info(@NotNull Supplier<String> message) {
        appender.append(compactMessage(message, Level.INFO));
    }

    /**
     * Log error messages.
     *
     * @param message {@link String} instance.
     */
    public void error(@NotNull Supplier<String> message) {
        appender.append(compactMessage(message, Level.ERROR));
    }

    /**
     * Log error messages.
     *
     * @param message   {@link String} instance.
     * @param exception {@link Throwable} instance.
     */
    public void error(@NotNull Supplier<String> message,
                      @NotNull Supplier<? extends Throwable> exception) {
        appender.append(compactMessage(message, Level.ERROR, exception));
    }

    /**
     * Log debug messages.
     *
     * @param message {@link String} instance.
     */
    public void debug(@NotNull Supplier<String> message) {
        appender.append(compactMessage(message, Level.DEBUG));
    }

    /**
     * Log warning messages.
     *
     * @param message {@link String} instance.
     */
    public void warn(@NotNull Supplier<String> message) {
        appender.append(compactMessage(message, Level.WARN));
    }

    /**
     * Log warning messages.
     *
     * @param message   {@link String} instance.
     * @param exception {@link Throwable} instance.
     */
    public void warn(@NotNull Supplier<String> message,
                     @NotNull Supplier<? extends Throwable> exception) {
        appender.append(compactMessage(message, Level.WARN, exception));
    }

    /**
     * Compact the message for reusability with minimum changes in the future
     * implementations.
     *
     * @param message actual {@link String} message to be logged.
     * @param level   {@link Level} of the log entry.
     * @return the initialized new {@link Message} instance which is immutable.
     */
    private Message compactMessage(@NotNull Supplier<String> message,
                                   @NotNull Level level) {
        return getMessage(appName, message, level, type, () -> null);
    }

    /**
     * Compact the message for reusability with minimum changes in the future
     * implementations.
     *
     * @param message   actual {@link String} message to be logged.
     * @param level     {@link Level} of the log entry.
     * @param throwable {@link Exception} is any error occurred.
     * @return the initialized new {@link Message} instance which is immutable.
     */
    private Message compactMessage(@NotNull Supplier<String> message,
                                   @NotNull Level level,
                                   @NotNull Supplier<? extends Throwable> throwable) {
        return getMessage(appName, message, level, type, throwable);
    }

    /**
     * Initialize new {@link Logger} instance base on the configuration provided
     * via a Application context.
     *
     * @return new {@link Logger} instance.
     */
    @NotNull
    public static Logger getLogger() {
        Objects.requireNonNull(appContext, """
                Application context is null. Check if the application runs of ApplicationContextAware environment.
                """);
        return appContext.getBean(Logger.class);
    }

    /**
     * Initialize new {@link Logger} instance base on the configuration provided
     * via a Application context and {@link String} qualifier parameter matches if
     * there are multiple {@link Logger} instances available.
     *
     * @param qualifier {@link String} bean name of the instance.
     * @return new {@link Logger} instance.
     */
    @NotNull
    public static Logger getLogger(@NotNull String qualifier) {
        Objects.requireNonNull(appContext, """
                Application context is null. Check if the application runs of ApplicationContextAware environment.
                """);
        return appContext.getBean(Logger.class, qualifier);
    }
}
