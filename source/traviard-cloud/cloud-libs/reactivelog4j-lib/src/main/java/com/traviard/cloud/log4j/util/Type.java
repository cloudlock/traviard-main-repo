package com.traviard.cloud.log4j.util;

import org.jetbrains.annotations.NotNull;

/**
 * Log message types includes here, indicating if the {@link Message}
 * is generating from a service, method or some other module component.
 * <p>
 * Basically, {@link Message} will be generated from a lower level class
 * or a configuration.
 *
 * @author Sachith Dickwella
 */
public enum Type {

    /**
     * Message originated from a class in a module.
     */
    CLASS("class"),
    /**
     * Message originated from a service endpoint,
     * and log from a controller.
     */
    SERVICE("service"),
    /**
     * Message originated from a HTTP endpoint, and
     * log from a filter or a any request interceptor
     * on the way.
     */
    HTTP("http");

    /**
     * {@link String} value for each of the {@link Type}.
     */
    private final String value;

    /**
     * Private constructor to initialize the enum with {@link String}
     * {@link #value}.
     *
     * @param value {@link String} type value.
     */
    Type(String value) {
        this.value = value;
    }

    /**
     * Get the {@link String} type {@link #value}.
     *
     * @return the instance {@link #value}
     */
    @NotNull
    public String value() {
        return this.value;
    }
}
