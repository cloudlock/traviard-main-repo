package com.traviard.cloud.log4j.appender;

import com.traviard.cloud.log4j.util.Message;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * This {@link ServiceAppender} invoke the web service endpoint provided
 * by properties or custom configuration.
 *
 * @author Sachith Dickwella
 */
public class ServiceAppender implements Appender {

    /**
     * Instance of load-balanced (ribbon enables) {@link WebClient} to
     * invoke service endpoints.
     */
    private final WebClient webClient;

    /**
     * Constructor to initialize {@link WebClient} instance {@link #webClient}.
     *
     * @param webClient instance of {@link WebClient}.
     */
    private ServiceAppender(WebClient webClient) {
        this.webClient = webClient;
    }

    /**
     * Initialize and return a new instance of {@link ServiceAppender} class.
     *
     * @return new instance of {@link ServiceAppender};
     */
    @NotNull
    public static Appender getInstance(@NotNull WebClient client) {
        return new ServiceAppender(client);
    }

    /**
     * Append the message according to the subsequent {@link Appender}
     * implementations.
     *
     * @param message a {@link Message} to append to corresponding appender.
     */
    @Override
    public void append(Message message) {
        webClient.post()
                .body(message, Message.class)
                .retrieve()
                .bodyToMono(ResponseEntity.class)
                .subscribe(ResponseEntity::getStatusCode);
    }
}
