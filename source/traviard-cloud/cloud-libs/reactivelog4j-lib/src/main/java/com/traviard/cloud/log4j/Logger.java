package com.traviard.cloud.log4j;

import com.traviard.cloud.log4j.util.Level;
import com.traviard.cloud.log4j.util.Message;
import com.traviard.cloud.log4j.util.Type;
import org.jetbrains.annotations.NotNull;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;
import static java.time.LocalDateTime.now;

/**
 * @author Sachith Dickwella
 */
public interface Logger {

    /**
     * Get the {@link java.lang.StackWalker.StackFrame} stream limit that
     * needs to get the call class.
     *
     * @return the {@link java.lang.StackWalker.StackFrame} stream limit
     * as an {@code int} value.
     */
    int stackFrameLimit();

    /**
     * Log information messages.
     *
     * @param message {@link String} instance.
     */
    void info(@NotNull Supplier<String> message);

    /**
     * Log error messages.
     *
     * @param message {@link String} instance.
     */
    void error(@NotNull Supplier<String> message);

    /**
     * Log error messages.
     *
     * @param message {@link String} instance.
     * @param exception  {@link Throwable} instance.
     */
    void error(@NotNull Supplier<String> message,
                               @NotNull Supplier<? extends Throwable> exception);

    /**
     * Log debug messages.
     *
     * @param message {@link String} instance.
     */
    void debug(@NotNull Supplier<String> message);

    /**
     * Log warning messages.
     *
     * @param message {@link String} instance.
     */
    void warn(@NotNull Supplier<String> message);

    /**
     * Log warning messages.
     *
     * @param message   {@link String} instance.
     * @param exception {@link Throwable} instance.
     */
    void warn(@NotNull Supplier<String> message,
                     @NotNull Supplier<? extends Throwable> exception);

    /**
     * Initialize and return new instance of {@link Message} class.
     *
     * @param appName   Name of the current application. This applies
     *                  to REST services.
     * @param message   {@link String} type {@link Supplier} message
     *                  to log with the entry.
     * @param level     {@link Level} of the current log entry.
     * @param type      {@link Type} of the current log entry.
     * @param exception {@link Throwable} type {@link Supplier} exception
     *                  to log with the entry.
     * @return an instance of {@link Message} build with params and stack
     * trace data.
     */
    default Message getMessage(final String appName,
                               final Supplier<String> message,
                               final Level level,
                               final Type type,
                               @NotNull final Supplier<? extends Throwable> exception) {
        var stackFrame = StackWalker.getInstance(RETAIN_CLASS_REFERENCE)
                .walk(stackFrameStream -> stackFrameStream
                        .limit(stackFrameLimit())
                        .reduce((first, second) -> second))
                .orElseThrow(() -> new NoSuchElementException("No stack frame element present"));

        return Message.builder()
                .dateTime(now())
                .level(level)
                .thread(Thread.currentThread().getName())
                .clazz(stackFrame.getDeclaringClass())
                .type(type)
                .method(stackFrame.getMethodName())
                .line(stackFrame.getLineNumber())
                .description(message)
                .applicationName(appName)
                .throwable(exception)
                .build();
    }
}
