package com.traviard.cloud.log4j.appender;

import com.traviard.cloud.log4j.util.Message;

/**
 * Abstraction for the Log appender. This would have multiple sub-classes
 * including the service appender.
 *
 * @author Sachith Dickwella
 * @see ServiceAppender
 */
@FunctionalInterface
public interface Appender {

    /**
     * Append the message according to the subsequent {@link Appender}
     * implementations.
     *
     * @param message a {@link Message} to append to corresponding appender.
     */
    void append(Message message);
}
