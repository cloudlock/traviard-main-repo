package com.traviard.cloud.log4j.logger;

import com.traviard.cloud.log4j.LogManager;
import com.traviard.cloud.log4j.appender.ServiceAppender;
import com.traviard.cloud.log4j.util.Type;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author Sachith Dickwella
 */
public class ServiceLogger extends LogManager {

    /**
     * Constructor to initialize super-class instances from
     * the sub-class.
     *
     * @param webClient to append messages to downstream target.
     * @param appName  of the module that logging started.
     * @param type     of the logging to distinguish where entry originated
     */
    public ServiceLogger(WebClient webClient, String appName, Type type) {
        super(ServiceAppender.getInstance(webClient), appName, type);
    }
}
