package com.traviard.cloud.reactor.config;

import com.traviard.cloud.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.WebFilter;

import java.net.URI;

/**
 * @author Sachith Dickwella
 */
@Configuration
public class WebRoutingConfig {

    /**
     * Application context path injected from the Spring context which specific
     * to each application.
     */
    @Value("${server.servlet.context-path}")
    private String contextPath;

    /**
     * Create and configure a {@link WebFilter} to add a context path to Aws client services
     * based on their {@code server.servlet.context-path} property on {@code application.yml}
     * file.
     *
     * @return a instance of {@link WebFilter} with valida context path
     */
    @Bean
    public WebFilter getWebFilterForContextPath() {
        return (exchange, chain) -> {
            final var request = exchange.getRequest();
            if (request.getURI().getPath().matches(String.format("%s/.*", contextPath))) {
                return chain.filter(exchange.mutate()
                        .request(request.mutate()
                                .contextPath(contextPath)
                                .build())
                        .build());
            }
            return chain.filter(exchange.mutate()
                    .request(request.mutate()
                            .uri(URI.create(StringUtils.EMPTY))
                            .build())
                    .build());
        };
    }
}
