package com.traviard.cloud.exhandler;

import com.traviard.cloud.exhandler.util.HttpErrorResponse;
import com.traviard.cloud.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sachith Dickwella
 */
public abstract class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Logger instance for logging.
     */
    protected final Logger logger;

    /**
     * Single-arg constructor from sub-type to initialize {@code logger} instance.
     *
     * @param logger {@link Logger} logger instance for logging
     */
    protected GlobalExceptionHandler(Logger logger) {
        this.logger = logger;
    }

    /**
     * Global exception handler for all checked exceptions.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any checked {@link Exception} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    @SuppressWarnings("unused")
    protected ResponseEntity<HttpErrorResponse> handleCheckedExceptions(HttpServletRequest request, Exception ex) {
        logger.error(() -> "", () -> ex);
        return getResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Global exception handler for service RuntimeExceptions.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    protected ResponseEntity<HttpErrorResponse> handleRuntimeException(HttpServletRequest request, RuntimeException ex) {

        logger.error(() -> "", () -> ex);
        return getResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * NullPointer exception handler for services. Typically identify as BAD_REQUEST handler.
     *
     * @param request {@link HttpServletRequest} instance that respective to the exception
     * @param ex      Any {@link RuntimeException} instance that could throws from services
     * @return Instance of {@link ResponseEntity}
     */
    protected ResponseEntity<HttpErrorResponse> handleNullPointerException(HttpServletRequest request, NullPointerException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        logger.error(status::getReasonPhrase, () -> ex);
        return getResponseEntity(request, status);
    }

    /**
     * Create {@link ResponseEntity} instance base on {@link HttpServletRequest}
     * and {@link HttpStatus} instances.
     *
     * @param request {@link HttpServletRequest} instance
     * @return {@link ResponseEntity} instance
     */
    protected ResponseEntity<HttpErrorResponse> getResponseEntity(@NotNull HttpServletRequest request,
                                                                  @NotNull HttpStatus status) {
        return ResponseEntity
                .status(status)
                .body(HttpErrorResponse
                        .builder()
                        .statusCode(status.value())
                        .cause(status.getReasonPhrase())
                        .protocol(request.getProtocol())
                        .endpoint(request.getRequestURI())
                        .build()
                );
    }
}