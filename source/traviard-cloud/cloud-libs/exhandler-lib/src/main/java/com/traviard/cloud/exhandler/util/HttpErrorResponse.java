package com.traviard.cloud.exhandler.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Sachith Dickwella
 */
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HttpErrorResponse implements Serializable {

    /**
     * Serialization version id.
     */
    private static final long serialVersionUID = 1577493233682639599L;

    private LocalDateTime timestamp;

    private String protocol;

    private Integer statusCode;

    private String cause;

    private String endpoint;

    private HttpErrorResponse() {
        this.timestamp = LocalDateTime.now();
    }

    /**
     * Constructor to be concerned by {@code Lombok} {@code @Builder}.
     */
    @SuppressWarnings("unused")
    @Builder
    public HttpErrorResponse(String protocol,
                             Integer statusCode,
                             String cause,
                             String endpoint) {
        this();
        this.protocol = protocol;
        this.statusCode = statusCode;
        this.cause = cause;
        this.endpoint = endpoint;
    }
}
