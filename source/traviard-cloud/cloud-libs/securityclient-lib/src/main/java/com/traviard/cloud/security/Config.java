package com.traviard.cloud.security;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.traviard.cloud.security.SecurityConstants.Cookies.CSRF_TOKEN;
import static com.traviard.cloud.security.SecurityConstants.Headers.X_AUTHORIZATION;

/**
 * @author Sachith Dickwella
 */
@Component
public class Config {

    /**
     * Register new {@link CookieCsrfTokenRepository} instance in the Bean
     * repository.
     *
     * @return new {@link CookieCsrfTokenRepository} instance
     */
    @Bean("csrfTokenRepo")
    public CsrfTokenRepository getCsrfTokenRepository() {
        final CookieCsrfTokenRepository ctr = new CookieCsrfTokenRepository();
        ctr.setCookieHttpOnly(true);
        ctr.setHeaderName(CSRF_TOKEN.name());
        ctr.setCookieName(CSRF_TOKEN.getName());
        ctr.setCookiePath(CSRF_TOKEN.getPath());

        return ctr;
    }

    /**
     * Get an empty {@link HttpEntity} object to send to downstream service to make secure request. This
     * function orchestrate a {@link HttpEntity} instance with a CSRF secure token and make the
     * parameterized {@link HttpCookie} value in to a custom header value before the downstream call.
     *
     * @param tokenCookie {@link HttpCookie} from the upstream http request
     * @return a {@link HttpEntity} object with CSRF and token cookie value header. Specifically,
     * {@code tokenCookie}'s value has embedded in to {@code X-Authorization} header
     */
    @NotNull
    public HttpHeaders validHeaders(@NotNull HttpCookie tokenCookie) {
        var token = getCsrfTokenRepository().generateToken(null).getToken();
        var headers = new HttpHeaders();

        headers.set(CSRF_TOKEN.name(), token);
        headers.set(HttpHeaders.COOKIE, String.format("%s=%s", CSRF_TOKEN.getName(), token));
        headers.set(X_AUTHORIZATION.getHeaderName(), tokenCookie.getValue());

        return headers;
    }

    /**
     * Get an empty {@link HttpEntity} object to send to downstream service to make secure request. This
     * function orchestrate a {@link HttpEntity} instance with a CSRF secure token and make the
     * parameterized {@link HttpCookie} value in to a custom header value before the downstream call.
     *
     * @param cookies from the upstream request
     * @return a {@link HttpEntity} object with CSRF and token cookie value header. Specifically,
     * {@code tokenCookie}'s value has embedded in to {@code X-Authorization} header
     */
    @NotNull
    public HttpHeaders validHeaders(@NotNull Set<HttpCookie> cookies) {
        final var headers = new HttpHeaders();
        for (var c : cookies) {
            if (c.getName().equals(CSRF_TOKEN.getName())) {
                headers.set(HttpHeaders.COOKIE, c.toString());
                headers.set(CSRF_TOKEN.name(), c.getValue());
            } else {
                headers.set(X_AUTHORIZATION.getHeaderName(), c.getValue());
            }
        }
        return headers;
    }
}
