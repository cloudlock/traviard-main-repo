package com.traviard.cloud.security.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author Sachith Dickwella
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {

    /**
     * Actual token value.
     */
    private String value;

    /**
     * This constructor is used by the JSON interpreter.
     */
    @SuppressWarnings("unused")
    public Token() {
        /*
         * Nothing goes here.
         */
    }

    /**
     * Single-args constructor to assign {@link #value} at creation.
     *
     * @param value actual token value.
     */
    public Token(String value) {
        this.value = value;
    }
}
