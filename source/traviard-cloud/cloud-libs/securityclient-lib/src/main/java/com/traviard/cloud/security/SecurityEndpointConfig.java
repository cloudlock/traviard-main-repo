package com.traviard.cloud.security;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author Sachith Dickwella
 */
@PropertySource("classpath:security-endpoint.properties")
@Getter
@Component
public class SecurityEndpointConfig {

    /**
     * Security service's User resource's base URL.
     */
    @Value("${gateway.security.service.endpoint.user.base}")
    private String userBaseURL;
    /**
     * Security service's Country resource's base URL.
     */
    @Value("${gateway.security.service.endpoint.country.base}")
    private String countryBaseURL;
}
