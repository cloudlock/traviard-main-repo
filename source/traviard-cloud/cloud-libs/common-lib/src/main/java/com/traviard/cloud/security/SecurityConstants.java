package com.traviard.cloud.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("java:S1214")
public interface SecurityConstants {

    interface Properties {
        /**
         * Security token validator service url property.
         */
        String PROPERTY_SECURITY_SERVICE_VALIDATE_TOKEN_URL = "security.service.url.http.validate-cookie";
        /**
         * Security token refresh service url property.
         */
        String PROPERTY_SECURITY_SERVICE_REFRESH_TOKEN_URL = "security.service.url.http.refresh-cookie";
        /**
         * Security service configuration property file name.
         */
        String FILE_SECURITY_CONFIG = "securityservice-config";
    }

    /**
     * Containes cookie definitions.
     */
    @AllArgsConstructor
    enum Cookies {
        /**
         * JWT security token cookie.
         */
        SECURITY_TOKEN("TOKEN", "/"),
        /**
         * CSRF token cookie.
         */
        CSRF_TOKEN("X-CSRF-TOKEN", "/");

        /**
         * Cookie name.
         */
        @Getter
        String name;
        /**
         * Cookie path
         */
        @Getter
        String path;
    }

    @AllArgsConstructor
    enum Headers {
        /**
         * Token authorization custom header.
         */
        X_AUTHORIZATION("X-Authorization");

        /**
         * Header name.
         */
        @Getter
        String headerName;
    }

    /**
     * Jwt token cookie's max-age in minutes.
     */
    Long COOKIE_MAX_AGE = 20L;
}
