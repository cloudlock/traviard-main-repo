package com.traviard.cloud.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * @author Sachith Dickwella
 */
public interface StringUtils {

    /**
     * Line-break string constant.
     */
    String LF = "\n";
    /**
     * Empty string constant.
     */
    String EMPTY = "";
    /**
     * Empty space string constant.
     */
    String EMPTY_SPACE = " ";

    /**
     * Strip off {@code null} {@link String} references to empty
     * strings or returns trimmed input.
     *
     * @param str {@link String} reference
     * @return stripped {@link String} reference
     */
    @SuppressWarnings("unused")
    static String strip(String str) {
        return Objects.isNull(str) ? EMPTY : str.trim();
    }

    /**
     * Return the status of the {@link String} if the String is null or holds empty value.
     *
     * @param str {@link String} value to check
     * @return the status of the {@link String}
     */
    static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    /**
     * Transform first character of the {@code str} value.
     *
     * @param str {@link String} input
     * @return transformed value
     */
    static String miniscule(@Nullable String str) {
        if (str != null && !str.isEmpty()) {
            if (str.length() > 2) {
                return Character.toLowerCase(str.charAt(0)) + str.substring(1);
            } else {
                return str.toLowerCase();
            }
        }
        return str;
    }
}
