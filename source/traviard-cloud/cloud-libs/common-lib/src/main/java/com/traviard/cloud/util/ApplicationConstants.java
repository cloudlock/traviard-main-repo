package com.traviard.cloud.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Sachith Dickwella
 */
@SuppressWarnings("java:S1214")
public interface ApplicationConstants {

    /**
     * Application domain name constant format string.
     */
    String CONST_FORMAT_DOMAIN_NAME = "%s@traviard.com";
    /**
     * File name not provided by the {@link org.springframework.context.annotation.PropertySource}
     * annotation.
     */
    String ERROR_FILENAME_NOT_PROVIDED = "File name not provided by '@PropertySource' annotation";

    /**
     * Append the domain name of the application to the parameter value
     * and return the {@link String} instance.
     *
     * @param userName Username {@link String} to append with domain name
     * @return appended username and domain name
     */
    static String appendDomain(String userName) {
        return String.format(CONST_FORMAT_DOMAIN_NAME, userName.toLowerCase());
    }

    /**
     * Common application related Header constants.
     */
    @Getter
    @AllArgsConstructor
    enum Headers {
        /**
         * Web application ID constant header.
         */
        X_APPLICATION_ID("X-Application-Id");

        /**
         * Actual header name on which sends to the browser.
         */
        private final String name;
    }

    /**
     * Common application query parameters to use across web applications.
     */
    @Getter
    @AllArgsConstructor
    enum QueryParamNames {
        /**
         * Web application ID param.
         */
        APPLICATION_ID_PARAM("n"),
        /**
         * Referer header value parameter.
         */
        REFERER_HEADER_PARAM("ref");

        /**
         * Actual Query parameter name.
         */
        private final String queryParamName;

        /**
         * Append query param and its value with {@code '='} character.
         *
         * @param param query parameter name
         * @param value query parameter value
         * @return the appended query parameter and value
         */
        @NotNull
        public static String appendParamValue(@NotNull QueryParamNames param, @Nullable String value) {
            return String.format("%s=%s", param.queryParamName, value);
        }

        /**
         * Append all query parameter combinations together with {@code '&'} and return as a single
         * {@link String} value.
         *
         * @param queryParamValues {@link List} of query parameter and value combinations
         * @return appended entire query parameter {@link String} value
         */
        @NotNull
        public static String appendQueryParams(@NotNull List<String> queryParamValues) {
            return queryParamValues.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining("&"));
        }
    }
}
