package com.traviard.cloud.kafka.util;

import org.jetbrains.annotations.NotNull;

/**
 * @author Sachith Dickwella
 */
public enum CompressionType {

    /**
     * Gzip is a method of compressing files (making them smaller)
     * for faster network transfers. It is also a file format.
     * <p>
     * Compression allows your web server to provide smaller file
     * sizes which load faster for your website users. Enabling gzip
     * compression is a standard practice.
     *
     * @implNote <link>https://en.wikipedia.org/wiki/Gzip</link>
     */
    GZIP("gzip"),
    /**
     * Snappy is a fast data compression and decompression library
     * written in C++ by Google based on ideas from LZ77 and open-sourced
     * in 2011. It does not aim for maximum compression, or compatibility
     * with any other compression library; instead, it aims for very high
     * speeds and reasonable compression.
     *
     * @implNote <link>https://en.wikipedia.org/wiki/Snappy_(compression)</link>
     */
    SNAPPY("snappy"),
    /**
     * LZ4 is a lossless data compression algorithm that is focused on
     * compression and decompression speed. It belongs to the LZ77 family
     * of byte-oriented compression schemes.
     *
     * @implNote <link>https://en.wikipedia.org/wiki/LZ4_(compression_algorithm)</link>
     */
    LZ4("lz4"),
    /**
     * Zstandard was designed to give a compression ratio comparable to that
     * of the DEFLATE algorithm (developed in 1991 and used in the original
     * ZIP and gzip programs), but faster, especially for decompression. It
     * is tunable with compression levels ranging from negative 5 (fastest)
     * to 22 (slowest in compression speed, but best compression ratio).
     * <p>
     * The zstd package includes parallel (multi-threaded) implementations of
     * both compression and decompression. Starting from version 1.3.2
     * (October 2017), zstd optionally implements very long range search and
     * deduplication similar to rzip or lrzip.
     * <p>
     * Compression speed can vary by a factor of 20 or more between the fastest
     * and slowest levels, while decompression is uniformly fast, varying by less
     * than 20% between the fastest and slowest levels.[5]
     * <p>
     * Zstd at its maximum compression level gives a compression ratio close to
     * lzma, lzham, and ppmx, and performs better than lza, or bzip2. Zstandard
     * reaches the current Pareto frontier, as it decompresses faster than any
     * other currently-available algorithm with similar or better compression ratio.
     * <p>
     * Dictionaries can have a large impact on the compression ratio of small
     * files, so Zstandard can use a user-provided compression dictionary. It also
     * offers a training mode, able to generate a dictionary from a set of samples.
     * In particular, one dictionary can be loaded to process large sets of files
     * with redundancy between files, but not necessarily within each file,
     * (example - log files).
     *
     * @implNote <link>https://en.wikipedia.org/wiki/Zstandard</link>
     */
    ZSTD("zstd");

    /**
     * Type value in {@link String}.
     */
    private final String type;

    /**
     * Enum constructor initialize.
     *
     * @param type of {@link String}.
     */
    CompressionType(String type) {
        this.type = type;
    }

    /**
     * Compression {@link #type} value.
     *
     * @return {@link String} value of {@link #type}.
     */
    public String type() {
        return this.type;
    }

    /**
     * Get the {@link CompressionType} value from it's
     * {@link String} counterpart.
     *
     * @return the {@link CompressionType} value.
     */
    @NotNull
    public static CompressionType of(@NotNull String type) {
        return switch (type.toLowerCase()) {
            case "gzip" -> GZIP;
            case "snappy" -> SNAPPY;
            case "lz4" -> LZ4;
            case "zstd" -> ZSTD;
            default -> throw new IllegalArgumentException(
                    String.format("Un-supported compression type : %s", type)
            );
        };
    }
}
