package com.traviard.cloud.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.Map;

/**
 * @author Sachith Dickwella
 */
@Configuration
public interface KafkaAdminConfig {

    /**
     * Get configuration for Kafka broker from the downstream
     * definitions.
     *
     * These configurations inject from the {@code kafka-config.properties}
     * file are originally declared in {@link org.apache.kafka.clients.admin.AdminClientConfig}
     * class and/or {@link org.apache.kafka.clients.CommonClientConfigs} class.
     *
     * More configs at :
     * <link>
     *     https://kafka.apache.org/documentation/#producerconfigs
     * </link>
     *
     * @return a {@link Map} of {@link String} keys and {@link Object} values.
     */
    Map<String, Object> configs();

    /**
     * Create a new {@link KafkaAdmin} instance and bind with Spring
     * bean definition.
     *
     * @return new {@link KafkaAdmin} instance.
     */
    @Bean
    default KafkaAdmin getKafkaAdmin() {
        return new KafkaAdmin(configs());
    }
}
