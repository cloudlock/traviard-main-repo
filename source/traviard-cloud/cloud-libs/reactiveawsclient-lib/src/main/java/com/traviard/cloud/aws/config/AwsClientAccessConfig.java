package com.traviard.cloud.aws.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.StorageClass;

/**
 * @author Sachith Dickwella
 */
@PropertySource("classpath:aws-config.properties")
@Configuration
public class AwsClientAccessConfig {

    /**
     * IAM users's access key value.
     */
    @Value("${app.client.aws.accessKey}")
    private String accessKey;
    /**
     * IAM users's secret key value.
     */
    @Value("${app.client.aws.secretKey}")
    private String secretAccessKey;
    /**
     * Load default region from the property file and cast it out to a {@link Region}
     * instance.
     */
    @Value("#{T(software.amazon.awssdk.regions.Region).of('${app.client.aws.default_region}')}")
    private Region defaultRegion;
    /**
     * Load default S3 object {@link StorageClass} class for DataStore application.
     */
    @Value("#{T(software.amazon.awssdk.services.s3.model.StorageClass).fromValue('${app.client.aws.default.s3.storage_class}')}")
    private StorageClass defaultStorageClass;

    /**
     * Bind new {@link AwsCredentialsProvider} instance to the Spring application context.
     *
     * @return new instance of {@link AwsCredentialsProvider}
     */
    @Bean
    public AwsCredentialsProvider getAwsCredentialProvider() {
        return StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretAccessKey));
    }

    /**
     * Bind new {@link S3Client} instance to the Spring application context.
     *
     * @param awsCredentialsProvider new instance of {@link AwsCredentialsProvider} from Spring
     *                               application context
     * @return new instance of {@link S3Client}
     */
    @Bean
    public S3Client getS3Client(@NotNull AwsCredentialsProvider awsCredentialsProvider) {
        return S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCredentialsProvider.resolveCredentials()))
                .region(defaultRegion)
                .build();
    }

    /**
     * Bind {@link #defaultRegion} instance to Spring context.
     *
     * @return the {@link #defaultRegion} instance
     */
    @Bean
    public Region getDefaultRegion() {
        return this.defaultRegion;
    }

    /**
     * Bind {@link #defaultStorageClass} instance to application context.
     *
     * @return the {@link #defaultStorageClass} instance
     */
    @Bean
    public StorageClass getDefaultStorageClass() {
        return this.defaultStorageClass;
    }
}
